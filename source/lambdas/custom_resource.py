"""Common modules"""
import logging
from os import environ

"""Specific modules required by this file"""
import requests
import json
# from boto3 import client

"""Set logging settings"""
logger = logging.getLogger()
log_level = 'INFO' if 'LOGGING_LEVEL' not in environ else environ['LOGGING_LEVEL'].upper()
level = logging.getLevelName(log_level)
logger.setLevel(level)

"""Set global variables"""
# cloudformation = client('cloudformation')



def lambda_handler(event, context):
    """The entrypoint for the Lambda.

    Parameters:
    ----------
    - event [dictionary]: The event received from CloudFormation.
    - context [dictionary]: The context of the event received from CloudFormation.

    Return:
    ------
    None
    """

    logger.debug(f'Received event: {event}')
    logger.debug(f'Received context: {context}')

    controller(event)

    return



def controller(event):
    """Analyzes the event from CloudFormation and decides what to do next.

    Parameters:
    ----------
    - event [dictionary]: The event received from CloudFormation.

    Return:
    ------
    None
    """

    request_type = event['RequestType']
    resource_properties = event['ResourceProperties']
    resource_properties_old = None if 'OldResourceProperties' not in event else event['OldResourceProperties']

    physical_resource_id = '' if 'PhysicalResourceId' not in event else event['PhysicalResourceId']

    update_params = {
        'Status': None,
        'Url': event['ResponseURL'],
        'StackId': event['StackId'],
        'RequestId': event['RequestId'],
        'LogicalResourceId': event['LogicalResourceId'],
        'PhysicalResourceId': physical_resource_id,
        'Metadata': {}
    }

    logger.debug(f'Update params: {update_params}')

    try:
        if event['ResourceType'] == 'Custom::IamSamlProvider':
            logger.debug('Resource type is IAM::SamlProvider')
            from resource_types.iam import saml_provider as custom_resource

        elif event['ResourceType'] == 'Custom::ConnectInstance':
            logger.debug('Resource type is Connect::Instance')
            from resource_types.connect import instance as custom_resource

        elif event['ResourceType'] == 'Custom::ConnectOriginAssociation':
            logger.debug('Resource type is Connect::OriginAssociation')
            from resource_types.connect import origin_association as custom_resource

        elif event['ResourceType'] == 'Custom::ConnectHoursOfOperation':
            logger.debug('Resource type is Connect::HoursOfOperation')
            from resource_types.connect import hours_of_operation as custom_resource

        elif event['ResourceType'] == 'Custom::ConnectQueue':
            logger.debug('Resource type is Connect::Queue')
            from resource_types.connect import queue as custom_resource

        elif event['ResourceType'] == 'Custom::ConnectRoutingProfile':
            logger.debug('Resource type is Connect::RoutingProfile')
            from resource_types.connect import routing_profile as custom_resource

        elif event['ResourceType'] == 'Custom::ConnectRoutingProfileQueue':
            logger.debug('Resource type is Connect::RoutingProfileQueue')
            from resource_types.connect import routing_profile_queue as custom_resource

        elif event['ResourceType'] == 'Custom::ConnectLambdaAssociation':
            logger.debug('Resource type is Connect::LambdaAssociation')
            from resource_types.connect import lambda_association as custom_resource

        # elif event['ResourceType'] == 'Custom::Connect::LexAssociation':
        #     logger.debug('Resource type is Connect::LexAssociation')
        #     from resource_types.connect import lex_association as custom_resource

        resource = custom_resource.manage_resource(request_type, resource_properties, resource_properties_old, physical_resource_id)

        if request_type != 'Delete':
            update_params['PhysicalResourceId'] = resource['Id'] if 'PhysicalResourceId' not in resource else resource['PhysicalResourceId']

            if 'Arn' in resource:
                update_params['Metadata'].update({ 'Arn': resource['Arn'] })

        update_params['Status'] = 'SUCCESS'

        logger.debug(f'New update params: {update_params}')

        update_cloudformation(**update_params)

    except Exception as e:
        update_params['Status'] = 'FAILED'
        logger.exception(f'Failed update params: {update_params}')
        update_cloudformation(**update_params)

    return



def update_cloudformation(**kwargs):
    """Sends update to CloudFormation with the status of the request.

    Parameters:
    ----------
    None

    kwargs:
    ----------
    - Status [string]: The status of the request, can be either SUCCESS or FAILED. Mandatory.
    - PhysicalResourceId [string]: The physical resource id of the resource, use empty string if none. Mandatory.
    - StackId [string]: The stack id of the CloudFormation stack. Mandatory.
    - RequestId [string]: The request id of the CloudFormation stack. Mandatory.
    - LogicalResourceId [string]: The logical resource id of the resource created. Mandatory.
    - Data [dictionary]: Additional metadata of the resource that can be obtained with GetAtt, use empty dictionary if none. Mandatory.

    - Reason [string][optional]: The status of the update, mandatory if the status is FAILED.

    Return:
    ------
    None
    """

    body = json.dumps({
        'Status': kwargs['Status'],
        'Reason': 'Review CloudWatch Logs for further details' if 'Reason' not in kwargs else kwargs['Reason'],
        'PhysicalResourceId': kwargs['PhysicalResourceId'],
        'StackId': kwargs['StackId'],
        'RequestId': kwargs['RequestId'],
        'LogicalResourceId': kwargs['LogicalResourceId'],
        'Data': kwargs['Metadata']
    })

    logger.debug(f'Request body: {body}')

    headers = {
        'content-type': 'application/json',
        'content-length': str(len(body))
    }

    logger.debug(f'Request headers: {headers}')

    try:
        response = requests.put(
            kwargs['Url'],
            data = body,
            headers = headers
        )

        if response.status_code == 200:
            logger.debug('Successfully submitted response to Cloudformation')
            return True

        else:
            raise Exception('Failed to submit CloudFormation response')

    except Exception as e:
        logger.exception(f'Failed to submit CloudFormation response')
        return False