"""Common modules"""
import logging
from os import environ

"""Specific modules required by this file"""
from boto3 import client
from botocore.config import Config
from uuid import UUID

"""Set logging settings"""
logger = logging.getLogger()
log_level = 'INFO' if 'LOGGING_LEVEL' not in environ else environ['LOGGING_LEVEL'].upper()
level = logging.getLevelName(log_level)
logger.setLevel(level)

"""Set global variables"""
config = Config(retries = { 'max_attempts': 10, 'mode': 'standard' })
connect = client('connect', config = config)



def __create_schedule(schedule_config):
    """Creates a list of schedules in the Amazon Connect Hours of Operation format.

    Parameters:
    ----------
    - schedule_config [dictionary]:
        Must contain the at least one of following keys:
            - MONDAY [list of strings]
            - TUESDAY [list of strings]
            - WEDNESDAY [list of strings]
            - THURSDAY [list of strings]
            - FRIDAY [list of strings]
            - SATURDAY [list of strings]
            - SUNDAY [list of strings]

        Each string in the list must be in the following format:
           24hourstarttimeformat-24hourendtimeformat

        For example:
            [ '09:00-17:00' ]
            [ '09:00-12:00', '14:00-17:00' ]

    Return:
    ------
    List (of schedule dictionaries in the Hours of Operation format)
    """

    logger.info('Creating list of schedules...')

    schedules = []
    for index, key in enumerate(schedule_config):
        logger.debug(f"Processing schedule: {key}")
        if schedule_config[key] is not None:
            for schedule_for_day in schedule_config[key]:
                schedules.append({
                    'Day': key.upper(),
                    'StartTime': {
                        'Hours': int(schedule_for_day.split('-')[0].split(':')[0]),
                        'Minutes': int(schedule_for_day.split('-')[0].split(':')[1])
                    },
                    'EndTime': {
                        'Hours': int(schedule_for_day.split('-')[1].split(':')[0]),
                        'Minutes': int(schedule_for_day.split('-')[1].split(':')[1])
                    }
                })

    logger.debug(f'Created list of schedules: {schedules}')

    return schedules



def manage_resource(action, resource_properties, resource_properties_old, physical_resource_id):
    """Handles the creation, update and deletion of Hours of Operations in an Amazon Connect instance.

    Parameters:
    ----------
    - action [string]: The action to perform. Must be one of Create, Update or Delete
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - Name [string]
            - Description [string]
            - TimeZone [string]
            - Monday [string][optional]
            - Tuesday [string][optional]
            - Wednesday [string][optional]
            - Thursday [string][optional]
            - Friday [string][optional]
            - Saturday [string][optional]
            - Sunday [string][optional]

        The Day(e.g. Monday, Tuesday, etc) must be in the following format:
            24hourstarttimeformat-24hourendtimeformat,24hourstarttimeformat-24hourendtimeformat

        For example:
            09:00-17:00
            09:00-12:00,14:00-17:00

    - resource_properties_old [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - Name [string]
            - Description [string]
            - TimeZone [string]
            - Monday [string][optional]
            - Tuesday [string][optional]
            - Wednesday [string][optional]
            - Thursday [string][optional]
            - Friday [string][optional]
            - Saturday [string][optional]
            - Sunday [string][optional]

        The Day(e.g. Monday, Tuesday, etc) must be in the following format:
            24hourstarttimeformat-24hourendtimeformat,24hourstarttimeformat-24hourendtimeformat

        For example:
            09:00-17:00
            09:00-12:00,14:00-17:00

        Set to None if no values are present.

    - physical_resource_id [string]: The Id of the Hours of Operation.

    Return:
    ------
    Dictionary (with Id and Arn of the Hours of Operation)
    """

    if action == 'Create':
        logger.debug('Creating new resource')
        response = create_resource(resource_properties)

    elif action == 'Update':
        logger.debug('Updating resource')
        response = update_resource(resource_properties, physical_resource_id)

    elif action == 'Delete':
        logger.debug('Deleting resource')
        response = delete_resource(resource_properties, physical_resource_id)

    else:
        raise Exception('unknown action')

    return response



def create_resource(resource_properties):
    """Creates an Hours of Operation in an Amazon Connect instance.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - Name [string]
            - Description [string]
            - TimeZone [string]
            - Monday [string][optional]
            - Tuesday [string][optional]
            - Wednesday [string][optional]
            - Thursday [string][optional]
            - Friday [string][optional]
            - Saturday [string][optional]
            - Sunday [string][optional]

        The Day(e.g. Monday, Tuesday, etc) must be in the following format:
            24hourstarttimeformat-24hourendtimeformat,24hourstarttimeformat-24hourendtimeformat

        For example:
            09:00-17:00
            09:00-12:00,14:00-17:00

    Return:
    ------
    Dictionary (with Id and Arn of the Hours of Operation)
    """

    try:
        instance_id = resource_properties['InstanceId']
        hours_of_operation_name = resource_properties['Name']
        hours_of_operation_description = resource_properties['Description']
        hours_of_operation_timezone = resource_properties['TimeZone']

    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    schedule_config = {}
    for day in [ 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY' ]:
        try:
            schedule_config.update({
                day: None if resource_properties[day.capitalize()].strip() == '' else resource_properties[day.capitalize()].split(',')
            })
        except Exception as e:
            schedule_config.update({
                day: None
            })


    response = connect.list_hours_of_operations(InstanceId = instance_id)
    existing_hours_of_operations = response['HoursOfOperationSummaryList']

    while True:
        if 'NextToken' in response:
            response = connect.list_hours_of_operations(
                InstanceId = instance_id,
                NextToken = response['NextToken']
            )
            existing_hours_of_operations.extend(response['HoursOfOperationSummaryList'])
        if 'NextToken' not in response:
            break

    hours_of_operation_params = {
        'InstanceId': instance_id,
        'Name': hours_of_operation_name,
        'Description': hours_of_operation_description,
        'TimeZone': hours_of_operation_timezone,
        'Config': __create_schedule(schedule_config)
    }

    hours_of_operation = None
    for existing_hours_of_operation in existing_hours_of_operations:
        if existing_hours_of_operation['Name'] == hours_of_operation_name:
            logger.debug('Existing Hours of Operation with the same name found...')
            hours_of_operation = existing_hours_of_operation
            break

    if hours_of_operation is None:
        logger.debug(f'Creating Hours of Operation with the following parameters: {hours_of_operation_params}')
        response = connect.create_hours_of_operation(**hours_of_operation_params)

        hours_of_operation = {
            'Id': response['HoursOfOperationId'],
            'Arn': response['HoursOfOperationArn']
        }

    else:
        hours_of_operation_params.update({ 'HoursOfOperationId': hours_of_operation['Id'] })
        logger.debug(f'Updating Hours of Operation with the following parameters: {hours_of_operation_params}')
        connect.update_hours_of_operation(**hours_of_operation_params)

    return {
        'Id': hours_of_operation['Id'],
        'Arn': hours_of_operation['Arn']
    }



def update_resource(resource_properties, physical_resource_id):
    """Updates an Hours of Operation in an Amazon Connect instance.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - Name [string]
            - Description [string]
            - TimeZone [string]
            - Monday [string][optional]
            - Tuesday [string][optional]
            - Wednesday [string][optional]
            - Thursday [string][optional]
            - Friday [string][optional]
            - Saturday [string][optional]
            - Sunday [string][optional]

        The Day(e.g. Monday, Tuesday, etc) must be in the following format:
            24hourstarttimeformat-24hourendtimeformat,24hourstarttimeformat-24hourendtimeformat

        For example:
            09:00-17:00
            09:00-12:00,14:00-17:00

    - physical_resource_id [string]: The Id of the Hours of Operation.

    Return:
    ------
    Dictionary (with Id and Arn of the Hours of Operation)
    """

    try:
        instance_id = resource_properties['InstanceId']
        hours_of_operation_name = resource_properties['Name']
        hours_of_operation_description = resource_properties['Description']
        hours_of_operation_timezone = resource_properties['TimeZone']

    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    schedule_config = {}
    for day in [ 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY' ]:
        try:
            schedule_config.update({
                day: None if resource_properties[day.capitalize()].strip() == '' else resource_properties[day.capitalize()].split(',')
            })
        except Exception as e:
            schedule_config.update({
                day: None
            })

    hours_of_operation = connect.describe_hours_of_operation(
        InstanceId = instance_id,
        HoursOfOperationId = physical_resource_id
    )['HoursOfOperation']

    hours_of_operation_params = {
        'InstanceId': instance_id,
        'HoursOfOperationId': physical_resource_id,
        'Name': hours_of_operation_name,
        'Description': hours_of_operation_description,
        'TimeZone': hours_of_operation_timezone,
        'Config': __create_schedule(schedule_config)
    }

    logger.debug(f'Updating Hours of Operation with the following parameters: {hours_of_operation_params}')
    connect.update_hours_of_operation(**hours_of_operation_params)

    return {
        'Id': hours_of_operation['HoursOfOperationId'],
        'Arn': hours_of_operation['HoursOfOperationArn']
    }



def delete_resource(resource_properties, physical_resource_id):
    """Deletes an Hours of Operation in an Amazon Connect instance.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]

    - physical_resource_id [string]: The Id of the Hours of Operation.

    Return:
    ------
    None
    """

    try:
        UUID(physical_resource_id)
    except ValueError or TypeError:
        return

    try:
        instance_id = resource_properties['InstanceId']
    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    connect.delete_hours_of_operation(
        InstanceId = instance_id,
        HoursOfOperationId = physical_resource_id
    )
    logger.debug('Hours of Operation deleted')

    return