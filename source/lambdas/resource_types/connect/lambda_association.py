"""Common modules"""
import logging
from os import environ

"""Specific modules required by this file"""
import random
import string
from boto3 import client
from botocore.config import Config

"""Set logging settings"""
logger = logging.getLogger()
log_level = 'INFO' if 'LOGGING_LEVEL' not in environ else environ['LOGGING_LEVEL'].upper()
level = logging.getLevelName(log_level)
logger.setLevel(level)

"""Set global variables"""
config = Config(retries = { 'max_attempts': 10, 'mode': 'standard' })
lambda_client = client('lambda', config = config)
connect = client('connect', config = config)



def manage_resource(action, resource_properties, resource_properties_old, physical_resource_id):
    """Handles the association and disassociation of Lambda with an Amazon Connect instance.

    Parameters:
    ----------
    - action [string]: The action to perform. Must be one of Create, Update or Delete
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - FunctionArn [string]

    - resource_properties_old [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - FunctionArn [string]

        Set to None if no values are present.

    - physical_resource_id [string]: The ARN of the associated Lambda

    Return:
    ------
    Dictionary (with Id and Arn of the Routing Profile)
    """

    if action == 'Create':
        logger.debug('Creating new resource')
        response = create_resource(resource_properties)

    elif action == 'Update':
        logger.debug('Updating resource')
        response = create_resource(resource_properties)
        delete_resource(resource_properties_old, physical_resource_id)

    elif action == 'Delete':
        logger.debug('Deleting resource')
        response = delete_resource(resource_properties, physical_resource_id)

    return response



def create_resource(resource_properties):
    """Associates a Lambda with in an Amazon Connect instance.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - FunctionArn [string]

    Return:
    ------
    Dictionary (with Id of the Lambda)
    """

    try:
        instance_id = resource_properties['InstanceId']
        function_arn = resource_properties['FunctionArn']
        permission_id = f"connect_{''.join(random.choices(string.ascii_uppercase + string.digits, k = 8))}-{instance_id}"
        region = function_arn.split(':')[3]
        account_id = function_arn.split(':')[4]
        instance_arn = f'arn:aws:connect:{region}:{account_id}:instance/{instance_id}'
    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    lambda_association_params = {
        'InstanceId': instance_id,
        'FunctionArn': function_arn,
    }

    response = connect.list_lambda_functions(InstanceId = instance_id)
    existing_lambda_associations = response['LambdaFunctions']

    while True:
        if 'NextToken' in response:
            response = connect.list_lambda_functions(InstanceId = instance_id, NextToken = response['NextToken'])
            existing_lambda_associations.extend(response['LambdaFunctions'])
        if 'NextToken' not in response:
            break

    lambda_function = None
    for existing_lambda_association in existing_lambda_associations:
        if existing_lambda_association == function_arn:
            logger.debug('Existing Lambda association with the same ARN found...')
            lambda_function = existing_lambda_association
            break

    if lambda_function is None:
        logger.debug(f'Associating new Lambda with the following parameters: {lambda_association_params}')
        response = connect.associate_lambda_function(**lambda_association_params)

        logger.info(f'Associated Lambda {function_arn} with instance {instance_id}')
    else:
        logger.info(f'Association with Lambda {function_arn} already present')

    return {
        'Id': function_arn
    }



def delete_resource(resource_properties, physical_resource_id):
    """Disassociates a Lambda with an Amazon Connect instance.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - FunctionArn [string]

    - physical_resource_id [string]: The ARN of the associated Lambda

    Return:
    ------
    Dictionary (with Id of the Lambda)
    """

    if resource_properties is None:
        return

    try:
        instance_id = resource_properties['InstanceId']
        function_arn = resource_properties['FunctionArn']
    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    lambda_association_params = {
        'InstanceId': instance_id,
        'FunctionArn': function_arn,
    }

    try:
        connect.disassociate_lambda_function(**lambda_association_params)
        logger.info(f'Disassociated Lambda {function_arn} with instance {instance_id}')
    except Exception as e:
        logger.exception(f'Failed to disassociated lambda {function_arn} with instance {instance_id}')

    return