"""Common modules"""
import logging
from os import environ

"""Specific modules required by this file"""
import random
import string
from boto3 import client
from botocore.config import Config
from uuid import UUID

"""Set logging settings"""
logger = logging.getLogger()
log_level = 'INFO' if 'LOGGING_LEVEL' not in environ else environ['LOGGING_LEVEL'].upper()
level = logging.getLevelName(log_level)
logger.setLevel(level)

"""Set global variables"""
config = Config(retries = { 'max_attempts': 10, 'mode': 'standard' })
connect = client('connect', config = config)



def __update_config(queue_params):
    """Updates the Queue in Amazon Connect.

    Parameters:
    ----------
    - queue_params [dictionary]:
        The parameters must contain the following keys:
            - InstanceId [string]
            - Name [string]
            - Description [string]
            - HoursOfOperationId [string]
            - MaxContacts [string]
            - Status [string]
            - OutboundCallerConfig [dcitionary]

    Return:
    ------
    Dictionary (with Id and Arn of the Queue)
    """

    queue = connect.describe_queue(
        InstanceId = queue_params['InstanceId'],
        QueueId = queue_params['QueueId']
    )['Queue']

    logger.debug(f'Current queue configuration: {queue}')

    if queue['Description'] != queue_params['Description'] or queue['Name'] != queue_params['Name']:
        connect.update_queue_name(
            InstanceId = queue_params['InstanceId'],
            QueueId = queue_params['QueueId'],
            Name = queue_params['Name'],
            Description = queue_params['Description']
        )
        logger.debug(f'Queue name and / or description updated')

    if queue['HoursOfOperationId'] != queue_params['HoursOfOperationId']:
        connect.update_queue_hours_of_operation(
            InstanceId = queue_params['InstanceId'],
            QueueId = queue_params['QueueId'],
            HoursOfOperationId = queue_params['HoursOfOperationId']
        )
        logger.debug(f'Queue hours of operation updated')

    if queue_params['MaxContacts'] is not None:
        if ('MaxContacts' in queue and queue['MaxContacts'] != queue_params['MaxContacts']) or ('MaxContacts' not in queue):
            connect.update_queue_max_contacts(
                InstanceId = queue_params['InstanceId'],
                QueueId = queue_params['QueueId'],
                MaxContacts = queue_params['MaxContacts']
            )
            logger.debug(f'Queue maximum number of contacts updated')

    if queue_params['OutboundCallerConfig']:
        # queue_outbound_config = __create_outbound_config(queue_params['InstanceId'], queue_params['OutboundCallerConfig'])

        if 'OutboundCallerConfig' in queue:
            changed_queue_outbound_config = {}

            if 'OutboundCallerIdName' in queue['OutboundCallerConfig']:
                if queue['OutboundCallerConfig']['OutboundCallerIdName'] != queue_params['OutboundCallerConfig']['OutboundCallerIdName']:
                    changed_queue_outbound_config.update({ 'OutboundCallerIdName': queue_params['OutboundCallerConfig']['OutboundCallerIdName'] })

            if 'OutboundCallerIdNumberId' in queue['OutboundCallerConfig']:
                if queue['OutboundCallerConfig']['OutboundCallerIdNumberId'] != queue_params['OutboundCallerConfig']['OutboundCallerIdNumberId']:
                    changed_queue_outbound_config.update({ 'OutboundCallerIdNumberId': queue_params['OutboundCallerConfig']['OutboundCallerIdNumberId'] })

            if 'OutboundFlowId' in queue['OutboundCallerConfig']:
                if queue['OutboundCallerConfig']['OutboundFlowId'] != queue_params['OutboundCallerConfig']['OutboundFlowId']:
                    changed_queue_outbound_config.update({ 'OutboundFlowId': queue_params['OutboundCallerConfig']['OutboundFlowId'] })

        else:
            changed_queue_outbound_config = queue_params['OutboundCallerConfig']

        connect.update_queue_outbound_caller_config(
            InstanceId = queue_params['InstanceId'],
            QueueId = queue_params['QueueId'],
            OutboundCallerConfig = changed_queue_outbound_config
        )
        logger.debug(f'Queue outbound configuration updated')

    return {
        'Id': queue['QueueId'],
        'Arn': queue['QueueArn']
    }



def manage_resource(action, resource_properties, resource_properties_old, physical_resource_id):
    """Handles the creation, update and deletion of Queues in an Amazon Connect instance.

    Parameters:
    ----------
    - action [string]: The action to perform. Must be one of Create, Update or Delete
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - Name [string]
            - Description [string]
            - HoursOfOperationId [string]
            - MaxContacts [string]
            - Status [string]
            - OutboundCallerIdName [string][optional]
            - OutboundCallerIdNumberId [string][optional]
            - OutboundFlowId [string][optional]

    - resource_properties_old [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - Name [string]
            - Description [string]
            - HoursOfOperationId [string]
            - MaxContacts [string]
            - Status [string]
            - OutboundCallerIdName [string][optional]
            - OutboundCallerIdNumberId [string][optional]
            - OutboundFlowId [string][optional]

        Set to None if no values are present.

    - physical_resource_id [string]: The Id of the Queue.

    Return:
    ------
    Dictionary (with Id and Arn of the Queue)
    """

    if action == 'Create':
        logger.debug('Creating new resource')
        response = create_resource(resource_properties)

    elif action == 'Update':
        logger.debug('Updating resource')
        response = update_resource(resource_properties, physical_resource_id)

    elif action == 'Delete':
        logger.debug('Deleting resource')
        response = delete_resource(resource_properties, physical_resource_id)

    else:
        raise Exception('unknown action')

    return response



def create_resource(resource_properties):
    """Creates a Queue in an Amazon Connect instance.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - Name [string]
            - Description [string]
            - HoursOfOperationId [string]
            - MaxContacts [string]
            - Status [string]
            - OutboundCallerIdName [string][optional]
            - OutboundCallerIdNumberId [string][optional]
            - OutboundFlowId [string][optional]

    Return:
    ------
    Dictionary (with Id and Arn of the Queue)
    """

    try:
        instance_id = resource_properties['InstanceId']
        queue_name = resource_properties['Name']
        queue_description = resource_properties['Description']
        queue_hours_of_operation_id = resource_properties['HoursOfOperationId']
        queue_status = resource_properties.get('Status', 'ENABLED').upper()

    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    try:
        queue_max_contacts = 0 if resource_properties['MaxContacts'].strip() == '' else int(resource_properties['MaxContacts'])
    except Exception as e:
        queue_max_contacts = 0

    queue_outbound_config = {}
    for key in [ 'OutboundCallerIdName', 'OutboundCallerIdNumberId', 'OutboundFlowId' ]:
        try:
            queue_outbound_config.update({
                key: None if resource_properties[key].strip() == '' else resource_properties[key].split(',')
            })
        except Exception as e:
            pass


    response = connect.list_queues(InstanceId = instance_id)
    existing_queues = [q for q in response['QueueSummaryList'] if q['QueueType'] == 'STANDARD']

    while True:
        if 'NextToken' in response:
            response = connect.list_queues(InstanceId = instance_id, NextToken = response['NextToken'])
            existing_queues.extend([q for q in response['QueueSummaryList'] if q['QueueType'] == 'STANDARD'])
        if 'NextToken' not in response:
            break

    queue = None
    for existing_queue in existing_queues:
        if existing_queue['Name'] == queue_name:
            logger.debug('Existing Queue with the same name found...')
            queue = existing_queue
            break

    queue_params = {
        'InstanceId': instance_id,
        'Name': queue_name,
        'Description': queue_description,
        'HoursOfOperationId': queue_hours_of_operation_id,
        'MaxContacts': queue_max_contacts,
        'OutboundCallerConfig': queue_outbound_config
    }

    if queue is None:
        logger.debug(f'Creating Queue with the following parameters: {queue_params}')
        response = connect.create_queue(**queue_params)

        queue = {
            'Id': response['QueueId'],
            'Arn': response['QueueArn']
        }

    else:
        queue_params.update({ 
            'QueueId': queue['Id']
        })

        logger.debug(f'Updating Queue with the following parameters: {queue_params}')
        __update_config(queue_params)

    connect.update_queue_status(**{
        'InstanceId': instance_id,
        'QueueId': queue['Id'],
        'Status': queue_status
    })

    return {
        'Id': queue['Id'],
        'Arn': queue['Arn']
    }



def update_resource(resource_properties, physical_resource_id):
    """Updates a Queue in an Amazon Connect instance.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - Name [string]
            - Description [string]
            - HoursOfOperationId [string]
            - MaxContacts [string]
            - Status [string]
            - OutboundCallerIdName [string][optional]
            - OutboundCallerIdNumberId [string][optional]
            - OutboundFlowId [string][optional]

    - physical_resource_id [string]: The Id of the Queue.

    Return:
    ------
    Dictionary (with Id and Arn of the Queue)
    """

    try:
        instance_id = resource_properties['InstanceId']
        queue_name = resource_properties['Name']
        queue_description = resource_properties['Description']
        queue_hours_of_operation_id = resource_properties['HoursOfOperationId']
        queue_status = resource_properties.get('Status', 'ENABLED').upper()

    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    try:
        queue_max_contacts = 0 if resource_properties['MaxContacts'].strip() == '' else int(resource_properties['MaxContacts'])
    except Exception as e:
        queue_max_contacts = 0

    queue_outbound_config = {}
    for key in [ 'OutboundCallerIdName', 'OutboundCallerIdNumberId', 'OutboundFlowId' ]:
        try:
            queue_outbound_config.update({
                key: None if resource_properties[key].strip() == '' else resource_properties[key].split(',')
            })
        except Exception as e:
            pass

    queue_params = {
        'InstanceId': instance_id,
        'QueueId': physical_resource_id,
        'Name': queue_name,
        'Description': queue_description,
        'HoursOfOperationId': queue_hours_of_operation_id,
        'MaxContacts': queue_max_contacts,
        'OutboundCallerConfig': queue_outbound_config
    }

    logger.debug(f'Updating Queue with the following parameters: {queue_params}')
    queue = __update_config(queue_params)

    connect.update_queue_status(**{
        'InstanceId': instance_id,
        'QueueId': queue['Id'],
        'Status': queue_status
    })

    return queue



def delete_resource(resource_properties, physical_resource_id):
    """Deletes a Queue in an Amazon Connect instance.
    Currently, the Amazon Connect API does not support deletion of Queues.
    This will rename the Queue by prefixing the name with a 'z' and update the description to note that it has been deleted by CloudFormation.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - Name [string]
            - Description [string]

    - physical_resource_id [string]: The Id of the Queue.

    Return:
    ------
    None
    """

    try:
        UUID(physical_resource_id)
    except ValueError or TypeError:
        return

    try:
        instance_id = resource_properties['InstanceId']
        queue_name = f"z {resource_properties['Name']} {''.join(random.choices(string.ascii_uppercase + string.digits, k = 8))}"
        queue_description = f"[DELETED BY CUSTOM CF] {resource_properties['Description']}"
    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    connect.update_queue_name(
        InstanceId = instance_id,
        QueueId = physical_resource_id,
        Name = queue_name,
        Description = queue_description
    )
    connect.update_queue_status(
        InstanceId = instance_id,
        QueueId = physical_resource_id,
        Status = 'DISABLED'
    )

    return