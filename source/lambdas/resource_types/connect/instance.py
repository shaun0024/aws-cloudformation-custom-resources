"""Common modules"""
import logging
from os import environ

"""Specific modules required by this file"""
from boto3 import client
from botocore.config import Config
from time import sleep
from uuid import UUID

"""Set logging settings"""
logger = logging.getLogger()
log_level = 'INFO' if 'LOGGING_LEVEL' not in environ else environ['LOGGING_LEVEL'].upper()
level = logging.getLevelName(log_level)
logger.setLevel(level)

"""Set global variables"""
max_retries = 10 if 'MAX_RETRIES' not in environ else int(environ['MAX_RETRIES'])
sleep_interval = 20 if 'SLEEP_INTERVAL' not in environ else int(environ['SLEEP_INTERVAL'])
config = Config(retries = { 'max_attempts': 10, 'mode': 'standard' })
connect = client('connect', config = config)



def __update_instance_attributes(instance_id, instance_attributes):
    """Updates Amazon Connect attributes. Currently supports the following:
    - INBOUND_CALLS
    - OUTBOUND_CALLS
    - CONTACTFLOW_LOGS
    - CONTACT_LENS
    - MULTI_PARTY_CONFERENCE

    Parameters:
    ----------
    - instance_id [string]: The Id of the Amazon Connect instance.
    - instance_attributes [dictionary]:
        A dictionary containing with the keys representing the attributes together with its value, which must be a string.
        E.g. {
            'INBOUND_CALLS': 'true',
            'OUTBOUND_CALLS': 'false',
            'CONTACTFLOW_LOGS': 'true',
            'CONTACT_LENS': 'true'
        }

    Return:
    ------
    None
    """

    for index, instance_attribute_key in enumerate(instance_attributes):
        attribute = connect.describe_instance_attribute(
            InstanceId = instance_id,
            AttributeType = instance_attribute_key
        )['Attribute']

        logger.debug(f'Current configuration for {instance_attribute_key}: {attribute}')

        if attribute['Value'] != instance_attributes[instance_attribute_key]:
            logger.debug(f"New configuration for attribute {instance_attribute_key}: {instance_attributes[instance_attribute_key]}")
            connect.update_instance_attribute(
                InstanceId = instance_id,
                AttributeType = instance_attribute_key,
                Value = instance_attributes[instance_attribute_key]
            )

    return



def __update_storage_config(instance_id, storage_configs):
    """Updates the storage configuration on Amazon Connect. Currently supports the following:
    - CALL_RECORDINGS
    - CHAT_TRANSCRIPTS
    - CONTACT_TRACE_RECORDS
    - AGENT_EVENTS
    - REAL_TIME_CONTACT_ANALYSIS_SEGMENTS

    Parameters:
    ----------
    - instance_id [string]: The Id of the Amazon Connect instance.
    - storage_configs [dictionary]:
        A dictionary containing with the keys representing the storage together with its value, which must be an object.
        E.g. {
            'CALL_RECORDINGS': {
                'StorageType': 'S3',
                'BucketName': "mybucket",
                'Prefix': "connect/myinstance/CallRecordings"
                'KeyId':
            },
            'CHAT_TRANSCRIPTS': {
                'StorageType': 'S3',
                'BucketName': "mybucket",
                'Prefix': "connect/myinstance/ChatTranscripts"
            },
            'CONTACT_TRACE_RECORDS': {
                'StorageType': 'Kinesis',
                'StreamArn': "arn:aws:kinesis:ap-southeast-2:123456789012:stream/amzconnect-ctr"
            },
            'AGENT_EVENTS': {
                'StorageType': 'Kinesis',
                'StreamArn': "arn:aws:kinesis:ap-southeast-2:123456789012:stream/amzconnect-agent-event"
            },
            'REAL_TIME_CONTACT_ANALYSIS_SEGMENTS': {
                'StorageType': 'Kinesis',
                'StreamArn': "arn:aws:kinesis:ap-southeast-2:123456789012:stream/amzconnect-contact-lens"
            }
        }

    Return:
    ------
    None
    """

    for index, storage_config_key in enumerate(storage_configs):
        storage_config = connect.list_instance_storage_configs(
            InstanceId = instance_id,
            ResourceType = storage_config_key
        )['StorageConfigs']

        if len(storage_config) == 0 and storage_configs[storage_config_key] is not None:
            """Associate new storage configuration if none detected"""

            association_params = {
                'InstanceId': instance_id,
                'ResourceType': storage_config_key,
                'StorageConfig': None
            }

            if storage_configs[storage_config_key]['StorageType'] == 'S3':
                storage_config = {
                    'StorageType': 'S3',
                    'S3Config': storage_configs[storage_config_key]['S3Config']
                }

            elif storage_configs[storage_config_key]['StorageType'] == 'Kinesis':
                storage_config = {
                    'StorageType': 'KINESIS_STREAM',
                    'KinesisStreamConfig': {
                        'StreamArn': storage_configs[storage_config_key]['StreamArn'],
                    }
                }

            association_params['StorageConfig'] = storage_config

            logger.debug(f"Associating new storage config: {association_params}")
            connect.associate_instance_storage_config(**association_params)

        elif len(storage_config) == 1:
            """Updates storage configuration if one is already present"""

            change_detected = False

            storage_config = storage_config[0]

            if storage_configs[storage_config_key] is None:
                logger.debug(f"Disassociating storage configuration {storage_config_key}: {storage_config['AssociationId']}")
                disassociation_params = {
                    'InstanceId': instance_id,
                    'AssociationId': storage_config['AssociationId'],
                    'ResourceType': storage_config_key
                }

                connect.disassociate_instance_storage_config(**disassociation_params)

            elif storage_configs[storage_config_key]['StorageType'] == 'S3':
                if (storage_config['S3Config']['BucketName'] != storage_configs[storage_config_key]['S3Config']['BucketName']):
                    logger.debug(f"Updating bucket from {storage_config['S3Config']['BucketName']} to {storage_configs[storage_config_key]['S3Config']['BucketName']}")
                    change_detected = True
                    storage_config['S3Config']['BucketName'] = storage_configs[storage_config_key]['S3Config']['BucketName']

                if (storage_config['S3Config']['BucketPrefix'] != storage_configs[storage_config_key]['S3Config']['BucketPrefix']):
                    logger.debug(f"Updating prefix from {storage_config['S3Config']['BucketPrefix']} to {storage_configs[storage_config_key]['S3Config']['BucketPrefix']}")
                    change_detected = True
                    storage_config['S3Config']['BucketPrefix'] = storage_configs[storage_config_key]['S3Config']['BucketPrefix']

                if (storage_config['S3Config']['EncryptionConfig']['KeyId'] != storage_configs[storage_config_key]['S3Config']['EncryptionConfig']['KeyId']):
                    logger.debug(f"Updating KMS from {storage_config['S3Config']['EncryptionConfig']['KeyId']} to {storage_configs[storage_config_key]['S3Config']['EncryptionConfig']['KeyId']}")
                    change_detected = True
                    storage_config['S3Config']['EncryptionConfig']['KeyId'] = storage_configs[storage_config_key]['S3Config']['EncryptionConfig']['KeyId']

            elif storage_configs[storage_config_key]['StorageType'] == 'Kinesis':
                if (storage_config['KinesisStreamConfig']['StreamArn'] != storage_configs[storage_config_key]['StreamArn']):
                    logger.debug(f"Updating stream from {storage_config['KinesisStreamConfig']['StreamArn']} to {storage_configs[storage_config_key]['StreamArn']}")
                    change_detected = True
                    storage_config['KinesisStreamConfig']['StreamArn'] = storage_configs[storage_config_key]['StreamArn']

            if change_detected is True:
                logger.debug(f'Changed params for {storage_config_key}: {storage_config}')
                association_params = {
                    'InstanceId': instance_id,
                    'AssociationId': storage_config['AssociationId'],
                    'ResourceType': storage_config_key,
                    'StorageConfig': storage_config
                }

                connect.update_instance_storage_config(**association_params)

            else:
                logger.debug(f'No changes detected for {storage_config_key}')

    return



def manage_resource(action, resource_properties, resource_properties_old, physical_resource_id):
    """Handles the creation, update and deletion of Amazon Connect instances.

    Parameters:
    ----------
    - action [string]: The type of action requested by CloudFormation. This must be Create, Update or Delete.
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - Alias
            - Directory
            - EnableContactFlowLogs
            - EnableContactLens
            - EnableInboundCalling
            - EnableOutboundCalling
            - KmsArn
            - BucketName
            - CallRecordingsPrefix
            - ChatTranscriptsPrefix
            - CtrStreamArn
            - AgentStreamArn

    - resource_properties_old [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - Alias
            - Directory
            - EnableContactFlowLogs
            - EnableContactLens
            - EnableInboundCalling
            - EnableOutboundCalling
            - KmsArn
            - BucketName
            - CallRecordingsPrefix
            - ChatTranscriptsPrefix
            - CtrStreamArn
            - AgentStreamArn

        Set to None if no values are present.

    - physical_resource_id [string]: The Amazon Connect id. If the action is create, this should be an empty string.

    Return:
    ------
    Dictionary
    """

    if action == 'Create':
        logger.debug('Creating new resource')
        response = create_resource(resource_properties)

    elif action == 'Update':
        logger.debug('Updating resource')
        response = update_resource(resource_properties, resource_properties_old, physical_resource_id)

    elif action == 'Delete':
        logger.debug('Deleting resource')
        response = delete_resource(physical_resource_id)

    return response



def create_resource(resource_properties):
    """Creates an Amazon Connect instance.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - Alias
            - Directory
            - EnableContactFlowLogs
            - EnableContactLens
            - EnableInboundCalling
            - EnableOutboundCalling
            - KmsArn
            - BucketName
            - CallRecordingsPrefix
            - ChatTranscriptsPrefix
            - CtrStreamArn
            - AgentStreamArn

    Return:
    ------
    Dictionary (with Id and Arn of the Amazon Connect Instance)
    """

    try:
        instance_alias = resource_properties['Alias']
        instance_directory = resource_properties['Directory']
        enable_contactflow_logs = resource_properties.get('EnableContactFlowLogs', 'true').lower()
        enable_contact_lens = resource_properties.get('EnableInboundCalling', 'false').lower()
        inbound_calling = resource_properties.get('EnableInboundCalling', 'false').lower()
        outbound_calling = resource_properties.get('EnableOutboundCalling', 'false').lower()
        enable_multi_party_conference = resource_properties.get('EnableMultiPartyConference', 'false').lower()
        instance_bucket_name = resource_properties['BucketName']
        call_recordings_prefix = resource_properties.get('CallRecordingsPrefix', 'CallRecordings')
        chat_transcripts_prefix = resource_properties.get('ChatTranscriptsPrefix', 'ChatTranscripts')

    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    try:
        kms_arn = None if resource_properties['KmsArn'].strip() == '' else resource_properties['KmsArn']
    except Exception as e:
        kms_arn = None

    try:
        ctr_stream_arn = None if resource_properties['CtrStreamArn'].strip() == '' else resource_properties['CtrStreamArn']
    except Exception as e:
        ctr_stream_arn = None

    try:
        agent_stream_arn = None if resource_properties['AgentStreamArn'].strip() == '' else resource_properties['AgentStreamArn']
    except Exception as e:
        agent_stream_arn = None

    try:
        contact_lens_stream_arn = None if resource_properties['ContactLensStreamArn'].strip() == '' else resource_properties['ContactLensStreamArn']
    except Exception as e:
        contact_lens_stream_arn = None

    response = connect.list_instances()
    existing_instances = response['InstanceSummaryList']

    while True:
        if 'NextToken' in response:
            response = connect.list_instances(NextToken = response['NextToken'])
            existing_instances.extend(response['InstanceSummaryList'])
        if 'NextToken' not in response:
            break

    instance_attributes = {
        'CONTACTFLOW_LOGS': enable_contactflow_logs,
        'CONTACT_LENS': enable_contact_lens,
        'MULTI_PARTY_CONFERENCE': enable_multi_party_conference
    }

    instance = None
    for existing_instance in existing_instances:
        if existing_instance['InstanceAlias'] == instance_alias:
            logger.debug(f'Existing instance with the same alias ({instance_alias}) found...')
            instance_attributes.update({
                'INBOUND_CALLS': inbound_calling,
                'OUTBOUND_CALLS': outbound_calling
            })

            instance = existing_instance

            break

    if instance is None:
        instance_params = {
            'InstanceAlias': instance_alias,
            'IdentityManagementType': instance_directory,
            'InboundCallsEnabled': False if inbound_calling == 'false' else True,
            'OutboundCallsEnabled': False if outbound_calling == 'false' else True
        }

        logger.debug(f'Creating instance with the following parameters: {instance_params}')
        instance = connect.create_instance(**instance_params)

    logger.debug(f'Instance details: {instance}')

    storage_configs = {
        'CALL_RECORDINGS': {
            'StorageType': 'S3',
            'S3Config': {
                'BucketName': instance_bucket_name,
                'BucketPrefix': f"connect/{instance_alias}/{call_recordings_prefix}"
            }
        },
        'CHAT_TRANSCRIPTS': {
            'StorageType': 'S3',
            'S3Config': {
                'BucketName': instance_bucket_name,
                'BucketPrefix': f"connect/{instance_alias}/{chat_transcripts_prefix}"
            }
        }
    }

    if kms_arn is not None:
        storage_configs['CALL_RECORDINGS']['S3Config'].update({
            'EncryptionConfig': {
                'EncryptionType': 'KMS',
                'KeyId': kms_arn
            }
        })

        storage_configs['CHAT_TRANSCRIPTS']['S3Config'].update({
            'EncryptionConfig': {
                'EncryptionType': 'KMS',
                'KeyId': kms_arn
            }
        })

    if ctr_stream_arn is not None:
        storage_configs.update({
            'CONTACT_TRACE_RECORDS': {
                'StorageType': 'Kinesis',
                'StreamArn': ctr_stream_arn
            }
        })

    if agent_stream_arn is not None:
        storage_configs.update({
            'AGENT_EVENTS': {
                'StorageType': 'Kinesis',
                'StreamArn': agent_stream_arn
            }
        })

    if contact_lens_stream_arn is not None:
        storage_configs.update({
            'REAL_TIME_CONTACT_ANALYSIS_SEGMENTS': {
                'StorageType': 'Kinesis',
                'StreamArn': contact_lens_stream_arn
            }
        })

    for x in range(max_retries):
        try:
            __update_instance_attributes(instance['Id'], instance_attributes)

            __update_storage_config(instance['Id'], storage_configs)
        except Exception as e:
            if x < max_retries:
                logger.exception('Instance may not be ready, sleeping before retrying...')
                sleep(sleep_interval)
            else:
                logger.exception('Instance created but one or more configuration failed')
                break

    logger.debug(f'Instance details: {instance}')

    return {
        'Id': instance['Id'],
        'Arn': instance['Arn']
    }



def update_resource(resource_properties, resource_properties_old, physical_resource_id):
    """Updates an Amazon Connect instance.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - Alias
            - EnableContactFlowLogs
            - EnableContactLens
            - EnableInboundCalling
            - EnableOutboundCalling
            - KmsArn
            - BucketName
            - CallRecordingsPrefix
            - ChatTranscriptsPrefix
            - CtrStreamArn
            - AgentStreamArn

    - physical_resource_id [string]: The Id of the Amazon Connect instance.

    Return:
    ------
    Dictionary (with Id and Arn of the Amazon Connect Instance)
    """

    instance = connect.describe_instance(InstanceId = physical_resource_id)['Instance']
    logger.debug(f'Instance details: {instance}')

    try:
        instance_alias = resource_properties['Alias']
        enable_contactflow_logs = resource_properties.get('EnableContactFlowLogs', 'true').lower()
        enable_contact_lens = resource_properties.get('EnableInboundCalling', 'false').lower()
        inbound_calling = resource_properties.get('EnableInboundCalling', 'false').lower()
        outbound_calling = resource_properties.get('EnableOutboundCalling', 'false').lower()
        enable_multi_party_conference = resource_properties.get('EnableMultiPartyConference', 'false').lower()
        instance_bucket_name = resource_properties['BucketName']
        call_recordings_prefix = resource_properties.get('CallRecordingsPrefix', 'CallRecordings')
        chat_transcripts_prefix = resource_properties.get('ChatTranscriptsPrefix', 'ChatTranscripts')

    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    try:
        kms_arn = None if resource_properties['KmsArn'].strip() == '' else resource_properties['KmsArn']
    except Exception as e:
        kms_arn = None

    try:
        ctr_stream_arn = None if resource_properties['CtrStreamArn'].strip() == '' else resource_properties['CtrStreamArn']
    except Exception as e:
        ctr_stream_arn = None

    try:
        agent_stream_arn = None if resource_properties['AgentStreamArn'].strip() == '' else resource_properties['AgentStreamArn']
    except Exception as e:
        agent_stream_arn = None

    try:
        contact_lens_stream_arn = None if resource_properties['ContactLensStreamArn'].strip() == '' else resource_properties['ContactLensStreamArn']
    except Exception as e:
        contact_lens_stream_arn = None

    instance_attributes = {
        'INBOUND_CALLS': inbound_calling,
        'OUTBOUND_CALLS': outbound_calling,
        'CONTACTFLOW_LOGS': enable_contactflow_logs,
        'CONTACT_LENS': enable_contact_lens,
        'MULTI_PARTY_CONFERENCE': enable_multi_party_conference
    }

    storage_configs = {
        'CALL_RECORDINGS': {
            'StorageType': 'S3',
            'S3Config': {
                'BucketName': instance_bucket_name,
                'BucketPrefix': f"connect/{instance_alias}/{call_recordings_prefix}"
            }
        },
        'CHAT_TRANSCRIPTS': {
            'StorageType': 'S3',
            'S3Config': {
                'BucketName': instance_bucket_name,
                'BucketPrefix': f"connect/{instance_alias}/{chat_transcripts_prefix}"
            }
        }
    }

    if kms_arn is not None:
        storage_configs['CALL_RECORDINGS']['S3Config'].update({
            'EncryptionConfig': {
                'EncryptionType': 'KMS',
                'KeyId': kms_arn
            }
        })

        storage_configs['CHAT_TRANSCRIPTS']['S3Config'].update({
            'EncryptionConfig': {
                'EncryptionType': 'KMS',
                'KeyId': kms_arn
            }
        })

    if ctr_stream_arn is not None:
        storage_configs.update({
            'CONTACT_TRACE_RECORDS': {
                'StorageType': 'Kinesis',
                'StreamArn': ctr_stream_arn
            }
        })

    else:
        storage_configs.update({
            'CONTACT_TRACE_RECORDS': None
        })

    if agent_stream_arn is not None:
        storage_configs.update({
            'AGENT_EVENTS': {
                'StorageType': 'Kinesis',
                'StreamArn': agent_stream_arn
            }
        })

    else:
        storage_configs.update({
            'AGENT_EVENTS': None
        })

    if contact_lens_stream_arn is not None:
        storage_configs.update({
            'REAL_TIME_CONTACT_ANALYSIS_SEGMENTS': {
                'StorageType': 'Kinesis',
                'StreamArn': contact_lens_stream_arn
            }
        })

    else:
        storage_configs.update({
            'REAL_TIME_CONTACT_ANALYSIS_SEGMENTS': None
        })

    __update_instance_attributes(instance['Id'], instance_attributes)

    __update_storage_config(instance['Id'], storage_configs)

    return {
        'Id': instance['Id'],
        'Arn': instance['Arn']
    }



def delete_resource(physical_resource_id):
    """Deletes an Amazon Connect instance.

    Parameters:
    ----------
    - physical_resource_id [string]: The Id of the Amazon Connect instance.

    Return:
    ------
    None
    """

    try:
        UUID(physical_resource_id)
    except ValueError or TypeError:
        return

    try:
        connect.delete_instance(InstanceId = physical_resource_id)
        logger.debug(f'Instance {physical_resource_id} deleted')
    except Exception as e:
        logger.exception(f'Instance {physical_resource_id} failed to be deleted')

    return