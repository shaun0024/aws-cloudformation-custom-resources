"""Common modules"""
import logging
from os import environ

"""Specific modules required by this file"""
from boto3 import client
from botocore.config import Config

"""Set logging settings"""
logger = logging.getLogger()
log_level = 'INFO' if 'LOGGING_LEVEL' not in environ else environ['LOGGING_LEVEL'].upper()
level = logging.getLevelName(log_level)
logger.setLevel(level)

"""Set global variables"""
config = Config(retries = { 'max_attempts': 10, 'mode': 'standard' })
connect = client('connect', config = config)



def manage_resource(action, resource_properties, resource_properties_old, physical_resource_id):
    """Handles the association and disassociation of security keys with an Amazon Connect instance.

    Parameters:
    ----------
    - action [string]: The action to perform. Must be one of Create, Update or Delete
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - PublicKey [string]

    - resource_properties_old [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - PublicKey [string]

        Set to None if no values are present.

    - physical_resource_id [string]: The association Id of the security key.

    Return:
    ------
    Dictionary (with Id and Arn of the Routing Profile)
    """

    if action == 'Create':
        logger.debug('Creating new resource')
        response = create_resource(resource_properties)

    elif action == 'Update':
        logger.debug('Updating resource')
        response = create_resource(resource_properties)
        delete_resource(resource_properties_old, physical_resource_id)

    elif action == 'Delete':
        logger.debug('Deleting resource')
        response = delete_resource(resource_properties, physical_resource_id)

    return response



def create_resource(resource_properties):
    """Associates a security key with an Amazon Connect instance.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - PublicKey [string]

    Return:
    ------
    Dictionary (with association Id of the security key)
    """

    try:
        instance_id = resource_properties['InstanceId']
        public_key = resource_properties['PublicKey']
    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    security_key_association_params = {
        'InstanceId': instance_id,
        'Key': public_key
    }

    response = connect.list_security_keys(InstanceId = instance_id)
    existing_security_key_associations = response['SecurityKeys']

    while True:
        if 'NextToken' in response:
            response = connect.list_security_keys(InstanceId = instance_id, NextToken = response['NextToken'])
            existing_security_key_associations.extend(response['SecurityKeys'])
        if 'NextToken' not in response:
            break

    security_key_association_id = None
    for existing_security_key_association in existing_security_key_associations:
        if existing_security_key_association['Key'] == public_key:
            logger.debug('Existing security key association with the same public key found...')
            security_key_association_id = existing_security_key_association['AssociationId']
            break

    if security_key_association_id is None:
        logger.debug(f'Associating new security key with the following parameters: {security_key_association_params}')
        security_key_association_id = connect.associate_security_key(**security_key_association_params)['AssociationId']

        logger.info(f'Associated security key with instance {instance_id}')

    else:
        logger.info(f'Association with security key with the same public key already present')

    return {
        'Id': security_key_association_id
    }



def delete_resource(resource_properties, physical_resource_id):
    """Disassociates a security key with an Amazon Connect instance.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - PublicKey [string]

    - physical_resource_id [string]: The association Id of the security key.

    Return:
    ------
    Dictionary (with Id of the Lambda)
    """

    if resource_properties is None:
        return

    try:
        instance_id = resource_properties['InstanceId']
    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    security_key_association_params = {
        'InstanceId': instance_id,
        'AssociationId': physical_resource_id
    }

    try:
        connect.disassociate_security_key(**security_key_association_params)
        logger.info(f'Disassociated security key with instance {instance_id}')
    except Exception as e:
        logger.exception(f'Failed to disassociate security key with instance {instance_id}')

    return