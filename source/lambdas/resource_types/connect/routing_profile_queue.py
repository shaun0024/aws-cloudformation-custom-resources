"""Common modules"""
import logging
from os import environ

"""Specific modules required by this file"""
from boto3 import client
from botocore.config import Config

"""Set logging settings"""
logger = logging.getLogger()
log_level = 'INFO' if 'LOGGING_LEVEL' not in environ else environ['LOGGING_LEVEL'].upper()
level = logging.getLevelName(log_level)
logger.setLevel(level)

"""Set global variables"""
config = Config(retries = { 'max_attempts': 10, 'mode': 'standard' })
connect = client('connect', config = config)



def manage_resource(action, resource_properties, resource_properties_old, physical_resource_id):
    """Handles the creation, update and deletion of Routing Profiles in an Amazon Connect instance.

    Parameters:
    ----------
    - action [string]: The action to perform. Must be one of Create, Update or Delete
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - ServiceToken [string]
            - InstanceId [string]
            - RoutingProfileId [string]
            - QueueId [string]
            - Channel [string]
            - Priority [string]
            - Delay [string]

    - resource_properties_old [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - ServiceToken [string]
            - InstanceId [string]
            - RoutingProfileId [string]
            - QueueId [string]
            - Channel [string]
            - Priority [string]
            - Delay [string]

        Set to None if no values are present.

    - physical_resource_id [string]: The Id of the Routing Profile.

    Return:
    ------
    Dictionary (with Id and Arn of the Routing Profile)
    """

    if action == 'Create':
        logger.debug('Creating new resource')
        response = create_resource(resource_properties)

    elif action == 'Update':
        logger.debug('Updating resource')
        response = create_resource(resource_properties)

    elif action == 'Delete':
        logger.debug('Deleting resource')
        response = delete_resource(resource_properties, physical_resource_id)

    return response



def create_resource(resource_properties):
    """Associates a Queue to a Routing Profile in an Amazon Connect instance.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - ServiceToken [string]
            - InstanceId [string]
            - RoutingProfileId [string]
            - QueueId [string]
            - Channel [string]
            - Priority [string]
            - Delay [string]

    Return:
    ------
    Dictionary (with Id and Arn of the Routing Profile)
    """

    try:
        aws_region = resource_properties['ServiceToken'].split(':')[3]
        aws_account_id = resource_properties['ServiceToken'].split(':')[4]
        instance_id = resource_properties['InstanceId']
        routing_profile_id = resource_properties['RoutingProfileId']
        routing_profile_queue_id = resource_properties['QueueId']
        routing_profile_queue_channel = resource_properties['Channel'].upper()
        routing_profile_queue_priority = int(resource_properties['Priority'])
        routing_profile_queue_delay = int(resource_properties['Delay'])
    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    routing_profile_queue_params = {
        'InstanceId': instance_id,
        'RoutingProfileId': routing_profile_id,
        'QueueConfigs': [
            {
                'QueueReference': {
                    'QueueId': routing_profile_queue_id,
                    'Channel': routing_profile_queue_channel
                },
                'Priority': routing_profile_queue_priority,
                'Delay': routing_profile_queue_delay
            }
        ]
    }

    response = connect.list_routing_profile_queues(InstanceId = instance_id, RoutingProfileId = routing_profile_id)
    existing_routing_profile_queues = response['RoutingProfileQueueConfigSummaryList']

    while True:
        if 'NextToken' in response:
            response = connect.list_routing_profile_queues(InstanceId = instance_id, NextToken = response['NextToken'])
            existing_routing_profile_queues.extend(response['RoutingProfileQueueConfigSummaryList'])
        if 'NextToken' not in response:
            break

    routing_profile_queue = None
    for existing_routing_profile_queue in existing_routing_profile_queues:
        if (existing_routing_profile_queue['QueueId'] == routing_profile_queue_id) and (existing_routing_profile_queue['Channel'] == routing_profile_queue_channel):
            logger.debug('Existing Routing Profile Queue with the same id found...')
            routing_profile_queue = existing_routing_profile_queue
            break

    if routing_profile_queue is None:
        logger.debug(f'Associating new Queue to Routing Profile with the following parameters: {routing_profile_queue_params}')
        response = connect.associate_routing_profile_queues(**routing_profile_queue_params)

        logger.info(f'Associated queue {routing_profile_queue_id} with channel {routing_profile_queue_channel} from routing profile {routing_profile_id}')

    else:
        logger.debug(f'Updating Queue in Routing Profile with the following parameters: {routing_profile_queue_params}')
        connect.update_routing_profile_queues(**routing_profile_queue_params)

        logger.info(f'Updated queue {routing_profile_queue_id} with channel {routing_profile_queue_channel} from routing profile {routing_profile_id}')

    return {
        'Id': f"arn:aws:connect:{aws_region}:{aws_account_id}:instance/{instance_id}/routing-profile/{routing_profile_id}/queue/{routing_profile_queue_id}/channel/{routing_profile_queue_channel.lower()}"
    }



def delete_resource(resource_properties, physical_resource_id):
    """Disassociates a Queue to a Routing Profile in an Amazon Connect instance.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - ServiceToken [string]
            - InstanceId [string]
            - RoutingProfileId [string]
            - QueueId [string]
            - Channel [string]
            - Priority [string]
            - Delay [string]

    - physical_resource_id [string]: The Id of the Routing Profile.

    Return:
    ------
    Dictionary (with Id and Arn of the Routing Profile)
    """

    if physical_resource_id is None or physical_resource_id == '':
        return

    try:
        instance_id = resource_properties['InstanceId']
        routing_profile_id = resource_properties['RoutingProfileId']
        routing_profile_queue_id = resource_properties['QueueId']
        routing_profile_queue_channel = resource_properties['Channel'].upper()
    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    routing_profile_queue_params = {
        'InstanceId': instance_id,
        'RoutingProfileId': routing_profile_id,
        'QueueReferences': [
            {
                'QueueId': routing_profile_queue_id,
                'Channel': routing_profile_queue_channel
            }
        ]
    }

    try:
        connect.disassociate_routing_profile_queues(**routing_profile_queue_params)
        logger.info(f'Disassociated queue {routing_profile_queue_id} with channel {routing_profile_queue_channel} from routing profile {routing_profile_id}')
    except Exception as e:
        logger.exception(f'Failed to disassociated queue {routing_profile_queue_id} with channel {routing_profile_queue_channel} from routing profile {routing_profile_id}')

    return