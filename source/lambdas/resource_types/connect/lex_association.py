"""Common modules"""
import logging
from os import environ

"""Specific modules required by this file"""
from boto3 import client
from botocore.config import Config

"""Set logging settings"""
logger = logging.getLogger()
log_level = 'INFO' if 'LOGGING_LEVEL' not in environ else environ['LOGGING_LEVEL'].upper()
level = logging.getLevelName(log_level)
logger.setLevel(level)

"""Set global variables"""
config = Config(retries = { 'max_attempts': 10, 'mode': 'standard' })
connect = client('connect', config = config)



def manage_resource(action, resource_properties, resource_properties_old, physical_resource_id):
    """Handles the association and disassociation of Lambda with an Amazon Connect instance.

    Parameters:
    ----------
    - action [string]: The action to perform. Must be one of Create, Update or Delete
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - LambdaArn [string]

    - resource_properties_old [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - LambdaArn [string]

        Set to None if no values are present.

    - physical_resource_id [string]: The Id of the Routing Profile.

    Return:
    ------
    Dictionary (with Id and Arn of the Routing Profile)
    """

    if action == 'Create':
        logger.debug('Creating new resource')
        response = create_resource(resource_properties)

    elif action == 'Update':
        logger.debug('Updating resource')
        response = create_resource(resource_properties)
        delete_resource(resource_properties_old, physical_resource_id)

    elif action == 'Delete':
        logger.debug('Deleting resource')
        response = delete_resource(resource_properties, physical_resource_id)

    return response



def create_resource(resource_properties):
    """Associates a Lambda with in an Amazon Connect instance.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]

            - LexVersion [string]

            - LexBotName [string]
            - LexBotRegion [string]

            - LexV2AliasArn [string]

    Return:
    ------
    Dictionary (with Id of the Lambda)
    """

    try:
        instance_id = resource_properties['InstanceId']
        lex_version = resource_properties['LexVersion'].upper()

        if lex_version == 'V1':
            lex_bot_name = resource_properties['LexBotName']
            lex_bot_region = resource_properties['LexBotRegion']

        if lex_version == 'V2':
            lex_v2_alias_arn = resource_properties['LexV2AliasArn']
    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    lex_association_params = { 'InstanceId': instance_id }

    if lex_version == 'V1':
        lex_association_params.update({
            'LexBot': {
                'Name': lex_bot_name,
                'LexRegion': lex_bot_region
            }
        })

    if lex_version == 'V2':
        lex_association_params.update({
            'LexV2Bot': {
                'AliasArn': lex_v2_alias_arn
            }
        })



    response = connect.list_bots(InstanceId = instance_id, LexVersion = lex_version)
    existing_lex_associations = response['LexBots']

    while True:
        if 'NextToken' in response:
            response = connect.list_bots(InstanceId = instance_id, LexVersion = lex_version, NextToken = response['NextToken'])
            existing_lex_associations.extend(response['LexBots'])
        if 'NextToken' not in response:
            break

    lex_association = None
    for existing_lex_association in existing_lex_associations:
        if lex_version == 'V1':
            if existing_lex_association['LexBot']['Name'] == lex_bot_name and existing_lex_association['LexBot']['LexRegion'] == lex_bot_region:
                logger.debug('Existing Lex association with the same name and region found...')
                lex_association = existing_lex_association['LexBot']
                break

            if existing_lex_association['LexV2Bot']['AliasArn'] == lex_v2_alias_arn:
                logger.debug('Existing Lex V2 association with the same name ARN found...')
                lex_association = existing_lex_association['LexV2Bot']
                break

    if lex_association is None:
        logger.debug(f'Associating new Lex with the following parameters: {lex_association_params}')
        response = connect.associate_bot(**lex_association_params)

        logger.info(f'Associated Lex {lex_bot_name} in region {lex_bot_region} with instance {instance_id}')

    else:
        logger.info(f'Association with Lex {lex_bot_name} in {lex_bot_region} already present')

    return {
        'Id': lex_bot_name
    }



def delete_resource(resource_properties, physical_resource_id):
    """Disassociates a Lambda with an Amazon Connect instance.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]

            - LexVersion [string]

            - LexBotName [string]
            - LexBotRegion [string]

            - LexV2AliasArn [string]

    - physical_resource_id [string]: The Id of the Routing Profile.

    Return:
    ------
    Dictionary (with Id of the Lambda)
    """

    if resource_properties is None:
        return

    try:
        instance_id = resource_properties['InstanceId']
        lex_version = resource_properties['LexVersion'].upper()

        if lex_version == 'V1':
            lex_bot_name = resource_properties['LexBotName']
            lex_bot_region = resource_properties['LexBotRegion']

        if lex_version == 'V2':
            lex_v2_alias_arn = resource_properties['LexV2AliasArn']
    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    lex_association_params = { 'InstanceId': instance_id }

    if lex_version == 'V1':
        lex_association_params.update({
            'LexBot': {
                'Name': lex_bot_name,
                'LexRegion': lex_bot_region
            }
        })

    if lex_version == 'V2':
        lex_association_params.update({
            'LexV2Bot': {
                'AliasArn': lex_v2_alias_arn
            }
        })

    try:
        connect.disassociate_bot(**lex_association_params)
        logger.info(f'Disassociated Lex')
    except Exception as e:
        logger.exception(f'Failed to disassociate Lex ')

    return