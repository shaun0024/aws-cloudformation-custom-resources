"""Common modules"""
import logging
from os import environ

"""Specific modules required by this file"""
import random
import string
from boto3 import client
from botocore.config import Config
from uuid import UUID

"""Set logging settings"""
logger = logging.getLogger()
log_level = 'INFO' if 'LOGGING_LEVEL' not in environ else environ['LOGGING_LEVEL'].upper()
level = logging.getLevelName(log_level)
logger.setLevel(level)

"""Set global variables"""
config = Config(retries = { 'max_attempts': 10, 'mode': 'standard' })
connect = client('connect', config = config)



def __update_config(routing_profile_params):
    """Updates the Routing Profile in Amazon Connect.

    Parameters:
    ----------
    - routing_profile_params [dictionary]:
        The parameters must contain the following keys:
            - InstanceId [string]
            - RoutingProfileId [string]
            - Description [string]
            - DefaultOutboundQueueId [string]
            - MediaConcurrencies [list of MediaConcurrency dictionaries]

    Return:
    ------
    Dictionary (with Id and Arn of the Routing Profile)
    """

    routing_profile = connect.describe_routing_profile(
        InstanceId = routing_profile_params['InstanceId'],
        RoutingProfileId = routing_profile_params['RoutingProfileId']
    )['RoutingProfile']
    logger.debug(f'Current routing profile configuration: {routing_profile}')


    if routing_profile['Description'] != routing_profile_params['Description'] or routing_profile['Name'] != routing_profile_params['Name']:
        connect.update_routing_profile_name(
            InstanceId = routing_profile_params['InstanceId'],
            RoutingProfileId = routing_profile_params['RoutingProfileId'],
            Description = routing_profile_params['Description']
        )
        logger.debug(f'Routing profile name and / or description updated')

    if routing_profile['DefaultOutboundQueueId'] != routing_profile_params['DefaultOutboundQueueId']:
        connect.update_routing_profile_default_outbound_queue(
            InstanceId = routing_profile_params['InstanceId'],
            RoutingProfileId = routing_profile_params['RoutingProfileId'],
            DefaultOutboundQueueId = routing_profile_params['DefaultOutboundQueueId']
        )
        logger.debug(f'Routing profile outbound queue updated')

    connect.update_routing_profile_concurrency(
        InstanceId = routing_profile_params['InstanceId'],
        RoutingProfileId = routing_profile_params['RoutingProfileId'],
        MediaConcurrencies = routing_profile_params['MediaConcurrencies']
    )
    logger.debug(f'Routing profile channel concurrency updated')

    return {
        'Id': routing_profile['RoutingProfileId'],
        'Arn': routing_profile['RoutingProfileArn']
    }



def manage_resource(action, resource_properties, resource_properties_old, physical_resource_id):
    """Handles the creation, update and deletion of Routing Profiles in an Amazon Connect instance.

    Parameters:
    ----------
    - action [string]: The action to perform. Must be one of Create, Update or Delete
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - Name [string]
            - Description [string]
            - OutboundQueueId [string]
            - VoiceConcurrency [integer][optional]
            - ChatConcurrency [integer][optional]
            - TaskConcurrency [integer][optional]

    - resource_properties_old [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - Name [string]
            - Description [string]
            - OutboundQueueId [string]
            - VoiceConcurrency [integer][optional]
            - ChatConcurrency [integer][optional]
            - TaskConcurrency [integer][optional]

        Set to None if no values are present.

    - physical_resource_id [string]: The Id of the Routing Profile.

    Return:
    ------
    Dictionary (with Id and Arn of the Routing Profile)
    """

    if action == 'Create':
        logger.debug('Creating new resource')
        response = create_resource(resource_properties)

    elif action == 'Update':
        logger.debug('Updating resource')
        response = update_resource(resource_properties, physical_resource_id)

    elif action == 'Delete':
        logger.debug('Deleting resource')
        response = delete_resource(resource_properties, physical_resource_id)

    else:
        raise Exception('unknown action')

    return response



def create_resource(resource_properties):
    """Creates a Routing Profile in an Amazon Connect instance.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - Name [string]
            - Description [string]
            - OutboundQueueId [string]
            - VoiceConcurrency [integer][optional]
            - ChatConcurrency [integer][optional]
            - TaskConcurrency [integer][optional]

    Return:
    ------
    Dictionary (with Id and Arn of the Routing Profile)
    """

    try:
        instance_id = resource_properties['InstanceId']
        routing_profile_name = resource_properties['Name']
        routing_profile_description = resource_properties['Description']
        routing_profile_outbound_queue_id = resource_properties['OutboundQueueId']

    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')


    media_concurrencies = []

    if 'VoiceConcurrency' in resource_properties and resource_properties['VoiceConcurrency'].strip() != '':
        media_concurrencies.append({
            'Channel': 'VOICE',
            'Concurrency': 1
        })

    if 'ChatConcurrency' in resource_properties and resource_properties['ChatConcurrency'].strip() != '':
        media_concurrencies.append({
            'Channel': 'CHAT',
            'Concurrency': int(resource_properties['ChatConcurrency'])
        })

    if 'TaskConcurrency' in resource_properties and resource_properties['TaskConcurrency'].strip() != '':
        media_concurrencies.append({
            'Channel': 'TASK',
            'Concurrency': int(resource_properties['TaskConcurrency'])
        })

    if not media_concurrencies:
        raise KeyError('At least one of VoiceConcurrency, ChatConcurrency or TaskConcurrency must be defined')

    response = connect.list_routing_profiles(InstanceId = instance_id)
    existing_routing_profiles = response['RoutingProfileSummaryList']

    while True:
        if 'NextToken' in response:
            response = connect.list_routing_profiles(InstanceId = instance_id, NextToken = response['NextToken'])
            existing_routing_profiles.extend(response['RoutingProfileSummaryList'])
        if 'NextToken' not in response:
            break

    routing_profile = None
    for existing_routing_profile in existing_routing_profiles:
        if existing_routing_profile['Name'] == routing_profile_name:
            logger.debug('Existing Routing Profile with the same name found...')
            routing_profile = existing_routing_profile
            break

    routing_profile_params = {
        'InstanceId': instance_id,
        'Name': routing_profile_name,
        'Description': routing_profile_description,
        'DefaultOutboundQueueId': routing_profile_outbound_queue_id,
        'MediaConcurrencies': media_concurrencies
    }

    if routing_profile is None:
        logger.debug(f'Creating Routing Profile with the following parameters: {routing_profile_params}')
        response = connect.create_routing_profile(**routing_profile_params)

        routing_profile = {
            'Id': response['RoutingProfileId'],
            'Arn': response['RoutingProfileArn']
        }

    else:
        routing_profile_params.update({ 'RoutingProfileId': routing_profile['Id'] })

        logger.debug(f'Updating Routing Profile with the following parameters: {routing_profile_params}')
        __update_config(routing_profile_params)

    return {
        'Id': routing_profile['Id'],
        'Arn': routing_profile['Arn']
    }



def update_resource(resource_properties, physical_resource_id):
    """Updates a Routing Profile in an Amazon Connect instance.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - Name [string]
            - Description [string]
            - OutboundQueueId [string]
            - VoiceConcurrency [integer][optional]
            - ChatConcurrency [integer][optional]
            - TaskConcurrency [integer][optional]

    - physical_resource_id [string]: The Id of the Routing Profile.

    Return:
    ------
    Dictionary (with Id and Arn of the Routing Profile)
    """

    try:
        instance_id = resource_properties['InstanceId']
        routing_profile_name = resource_properties['Name']
        routing_profile_description = resource_properties['Description']
        routing_profile_outbound_queue_id = resource_properties['OutboundQueueId']

    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    media_concurrencies = []
    if 'VoiceConcurrency' in resource_properties and resource_properties['VoiceConcurrency'].strip() != '':
        media_concurrencies.append({
            'Channel': 'VOICE',
            'Concurrency': 1
        })

    if 'ChatConcurrency' in resource_properties and resource_properties['ChatConcurrency'].strip() != '':
        media_concurrencies.append({
            'Channel': 'CHAT',
            'Concurrency': int(resource_properties['ChatConcurrency'])
        })

    if 'TaskConcurrency' in resource_properties and resource_properties['TaskConcurrency'].strip() != '':
        media_concurrencies.append({
            'Channel': 'TASK',
            'Concurrency': int(resource_properties['TaskConcurrency'])
        })

    if not media_concurrencies:
        raise KeyError('At least one of VoiceConcurrency, ChatConcurrency or TaskConcurrency must be defined')

    routing_profile_params = {
        'InstanceId': instance_id,
        'RoutingProfileId': physical_resource_id,
        'Name': routing_profile_name,
        'Description': routing_profile_description,
        'DefaultOutboundQueueId': routing_profile_outbound_queue_id,
        'MediaConcurrencies': media_concurrencies
    }

    logger.debug(f'Updating Routing Profile with the following parameters: {routing_profile_params}')
    routing_profile = __update_config(routing_profile_params)

    return routing_profile



def delete_resource(resource_properties, physical_resource_id):
    """Deletes a Routing Profile in an Amazon Connect instance.
    Currently, the Amazon Connect API does not support deletion of Routing Profiles.
    This will rename the Routing Profile by prefixing the name with a 'z' and update the description to note that it has been deleted by CloudFormation.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - Name [string]
            - Description [string]

    - physical_resource_id [string]: The Id of the Routing Profile.

    Return:
    ------
    Dictionary (with Id and Arn of the Routing Profile)
    """

    try:
        UUID(physical_resource_id)
    except ValueError or TypeError:
        return

    try:
        instance_id = resource_properties['InstanceId']
        routing_profile_name = f"z {resource_properties['Name']} {''.join(random.choices(string.ascii_uppercase + string.digits, k = 8))}"
        routing_profile_description = f"[DELETED BY CUSTOM CF] {resource_properties['Description']}"
    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    connect.update_routing_profile_name(
        InstanceId = instance_id,
        RoutingProfileId = physical_resource_id,
        Name = routing_profile_name,
        Description = routing_profile_description
    )

    return