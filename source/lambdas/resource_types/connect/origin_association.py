"""Common modules"""
import logging
from os import environ

"""Specific modules required by this file"""
from boto3 import client
from botocore.config import Config

"""Set logging settings"""
logger = logging.getLogger()
log_level = 'INFO' if 'LOGGING_LEVEL' not in environ else environ['LOGGING_LEVEL'].upper()
level = logging.getLevelName(log_level)
logger.setLevel(level)

"""Set global variables"""
config = Config(retries = { 'max_attempts': 10, 'mode': 'standard' })
connect = client('connect', config = config)



def manage_resource(action, resource_properties, resource_properties_old, physical_resource_id):
    """Handles the association and disassociation of approved origins with an Amazon Connect instance.

    Parameters:
    ----------
    - action [string]: The action to perform. Must be one of Create, Update or Delete
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - Origin [string]

    - resource_properties_old [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - Origin [string]

        Set to None if no values are present.

    - physical_resource_id [string]: The Id of the origin.

    Return:
    ------
    Dictionary (with Id and Arn of the Routing Profile)
    """

    if action == 'Create':
        logger.debug('Creating new resource')
        response = create_resource(resource_properties)

    elif action == 'Update':
        logger.debug('Updating resource')
        response = create_resource(resource_properties)
        delete_resource(resource_properties_old, physical_resource_id)

    elif action == 'Delete':
        logger.debug('Deleting resource')
        response = delete_resource(resource_properties, physical_resource_id)

    return response



def create_resource(resource_properties):
    """Associates an origin with in an Amazon Connect instance.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - Origin [string]

    Return:
    ------
    Dictionary (with Id of the origin)
    """

    try:
        instance_id = resource_properties['InstanceId']
        origin = resource_properties['Origin']
    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    origin_association_params = {
        'InstanceId': instance_id,
        'Origin': origin,
    }

    response = connect.list_approved_origins(InstanceId = instance_id)
    existing_approved_origins = response['Origins']

    while True:
        if 'NextToken' in response:
            response = connect.list_approved_origins(InstanceId = instance_id, NextToken = response['NextToken'])
            existing_approved_origins.extend(response['Origins'])
        if 'NextToken' not in response:
            break

    approved_origin = None
    for existing_approved_origin in existing_approved_origins:
        if existing_approved_origin == origin:
            logger.debug('Existing origin association with the same URL found...')
            approved_origin = existing_approved_origin
            break

    if approved_origin is None:
        logger.debug(f'Associating new origin with the following parameters: {origin_association_params}')
        response = connect.associate_approved_origin(**origin_association_params)

        logger.info(f'Associated origin {origin} with instance {instance_id}')

    else:
        logger.info(f'Association with origin {origin} already present')

    return {
        'Id': origin
    }



def delete_resource(resource_properties, physical_resource_id):
    """Disassociates an origin with an Amazon Connect instance.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - InstanceId [string]
            - Origin [string]

    - physical_resource_id [string]: The URL of the origin

    Return:
    ------
    Dictionary (with Id of the origin)
    """

    if resource_properties is None:
        return

    try:
        instance_id = resource_properties['InstanceId']
        origin = resource_properties['Origin']
    except Exception as e:
        logger.exception(f'One or more keys were not found in resource properties, failed to disassociate origin')
        return

    origin_association_params = {
        'InstanceId': instance_id,
        'Origin': origin,
    }

    try:
        connect.disassociate_approved_origin(**origin_association_params)
        logger.info(f'Disassociated origin {origin} with instance {instance_id}')
    except Exception as e:
        logger.exception(f'Failed to disassociate origin {origin} with instance {instance_id}')

    return