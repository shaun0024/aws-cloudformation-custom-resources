"""Common modules"""
import logging
from os import environ

"""Specific modules required by this file"""
import json
import requests
from boto3 import client, resource
from botocore.config import Config

"""Set logging settings"""
logger = logging.getLogger()
log_level = 'INFO' if 'LOGGING_LEVEL' not in environ else environ['LOGGING_LEVEL'].upper()
level = logging.getLevelName(log_level)
logger.setLevel(level)

"""Set global variables"""
config = Config(retries = { 'max_attempts': 10, 'mode': 'standard' })
iam = client('iam', config = config)



def manage_resource(action, resource_properties, resource_properties_old, physical_resource_id):
    """Handles the management of SAML Providers in IAM.

    Parameters:
    ----------
    - action [string]: The action to perform. Must be one of Create, Update or Delete
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - Name [string]
            - SamlMetadataDocumentUrl [string]
            - SamlMetadataDocumentBucket [string]
            - SamlMetadataDocumentKey [string]
            - Tags [string]

    - resource_properties_old [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - Name [string]
            - SamlMetadataDocumentUrl [string]
            - SamlMetadataDocumentBucket [string]
            - SamlMetadataDocumentKey [string]
            - Tags [string]

        Set to None if no values are present.

    - physical_resource_id [string]: The Id of the Routing Profile.

    Return:
    ------
    Dictionary (with Id and Arn of the Routing Profile)
    """

    if action == 'Create':
        logger.debug('Creating new resource')
        response = create_resource(resource_properties)

    elif action == 'Update':
        logger.debug('Updating resource')
        response = update_resource(resource_properties, physical_resource_id)

    elif action == 'Delete':
        logger.debug('Deleting resource')
        response = delete_resource(physical_resource_id)

    return response



def create_resource(resource_properties):
    """Creates a new SAML Provider in IAM.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - Name [string]
            - SamlMetadataDocumentUrl [string]
            - SamlMetadataDocumentBucket [string]
            - SamlMetadataDocumentKey [string]
            - Tags [string]

    Return:
    ------
    Dictionary (with Id of the SAML Provider)
    """

    try:
        provider_name = resource_properties['Name']
        # saml_metadata_url = None if 'SamlMetadataDocumentUrl' not in resource_properties else resource_properties['SamlMetadataDocumentUrl']
        saml_metadata_url = resource_properties.get('SamlMetadataDocumentUrl', None)
        # saml_metadata_document_bucket = None if 'SamlMetadataDocumentBucket' not in resource_properties else resource_properties['SamlMetadataDocumentBucket']
        saml_metadata_document_bucket = resource_properties.get('SamlMetadataDocumentBucket', None)
        # saml_metadata_document_key = None if 'SamlMetadataDocumentKey' not in resource_properties else resource_properties['SamlMetadataDocumentKey']
        saml_metadata_document_key = resource_properties.get('SamlMetadataDocumentKey', None)
        tags = None if 'Tags' not in resource_properties else json.loads(resource_properties['Tags'])
    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    if (saml_metadata_url is None) and (saml_metadata_document_bucket is None and saml_metadata_document_key is None):
        raise KeyError(f'One or more keys were not found in resource properties')

    if (saml_metadata_url is not None) and (saml_metadata_document_bucket is not None and saml_metadata_document_key is not None):
        raise Exception(f'Only one of url or bucket is required')

    if (saml_metadata_document_bucket is None and saml_metadata_document_key is not None) or (saml_metadata_document_bucket is not None and saml_metadata_document_key is None):
        raise Exception(f'Both bucket and key name must be provided')

    if saml_metadata_url is not None:
        response = requests.get(saml_metadata_url)
        if response.status_code >= 200 and response.status_code <= 299:
            saml_metadata_document = response.content.decode('utf-8-sig')

    if saml_metadata_document_bucket is not None and saml_metadata_document_key is not None:
        saml_metadata_document_key_data = saml_metadata_document_key.split(':')

        s3_object = resource('s3').Object(saml_metadata_document_bucket, saml_metadata_document_key_data[0])
        if len(saml_metadata_document_key_data) > 1:
            saml_metadata_document = s3_object.get(VersionId = saml_metadata_document_key_data[1])['Body'].read().decode('utf-8')
        else:
            saml_metadata_document = s3_object.get()['Body'].read().decode('utf-8')

    saml_provider_params = {
        'Name': provider_name,
        'SAMLMetadataDocument': saml_metadata_document,
    }

    if tags is not None:
        list_of_tags = []
        for index, key in enumerate(tags):
            list_of_tags.append({
                'Key': key,
                'Value': tags[key]
            })

        saml_provider_params.update({ 'Tags': list_of_tags })

    saml_provider_arn = iam.create_saml_provider(**saml_provider_params)['SAMLProviderArn']

    return {
        'Id': saml_provider_arn
    }



def update_resource(resource_properties, physical_resource_id):
    """Updates a SAML Provider in IAM.

    Parameters:
    ----------
    - resource_properties [dictionary]:
        The properties received from CloudFormation and must contain the following keys:
            - Name [string]
            - SamlMetadataDocumentUrl [string]
            - SamlMetadataDocumentBucket [string]
            - SamlMetadataDocumentKey [string]
            - Tags [string]

    - physical_resource_id [string]: The Id of the Routing Profile.

    Return:
    ------
    Dictionary (with Id of the SAML Provider)
    """

    try:
        # provider_name_from_arn = physical_resource_id.split(':')[-1].split('/')[-1]
        # provider_name = resource_properties['Name']
        # saml_metadata_url = None if 'SamlMetadataDocumentUrl' not in resource_properties else resource_properties['SamlMetadataDocumentUrl']
        # saml_metadata_document_bucket = None if 'SamlMetadataDocumentBucket' not in resource_properties else resource_properties['SamlMetadataDocumentBucket']
        # saml_metadata_document_key = None if 'SamlMetadataDocumentKey' not in resource_properties else resource_properties['SamlMetadataDocumentKey']


        provider_name_from_arn = physical_resource_id.split(':')[-1].split('/')[-1]
        provider_name = resource_properties['Name']
        saml_metadata_url = resource_properties.get('SamlMetadataDocumentUrl', None)
        saml_metadata_document_bucket = resource_properties.get('SamlMetadataDocumentBucket', None)
        saml_metadata_document_key = resource_properties.get('SamlMetadataDocumentKey', None)



    except Exception as e:
        raise KeyError(f'One or more keys were not found in resource properties')

    if (saml_metadata_url is None) and (saml_metadata_document_bucket is None and saml_metadata_document_key is None):
        raise KeyError(f'One or more keys were not found in resource properties')

    if (saml_metadata_url is not None) and (saml_metadata_document_bucket is not None and saml_metadata_document_key is not None):
        raise Exception(f'Only one of url or bucket is required')

    if (saml_metadata_document_bucket is None and saml_metadata_document_key is not None) or (saml_metadata_document_bucket is not None and saml_metadata_document_key is None):
        raise Exception(f'Only one of url or bucket is required')

    if provider_name_from_arn != provider_name:
        saml_provider_arn = create_resource(resource_properties)['Id']
        delete_resource(physical_resource_id)

    else:
        if saml_metadata_url is not None:
            response = requests.get(saml_metadata_url)
            if response.status_code >= 200 and response.status_code <= 299:
                saml_metadata_document = response.content.decode('utf-8-sig')

        if saml_metadata_document_bucket is not None and saml_metadata_document_key is not None:
            saml_metadata_document_key_data = saml_metadata_document_key.split(':')

            s3_object = resource('s3').Object(saml_metadata_document_bucket, saml_metadata_document_key_data[0])
            if len(saml_metadata_document_key_data) > 1:
                saml_metadata_document = s3_object.get(VersionId = saml_metadata_document_key_data[1])['Body'].read().decode('utf-8')
            else:
                saml_metadata_document = s3_object.get()['Body'].read().decode('utf-8')

        saml_provider_params = {
            'SAMLProviderArn': physical_resource_id,
            'SAMLMetadataDocument': saml_metadata_document
        }

        saml_provider_arn = iam.update_saml_provider(**saml_provider_params)['SAMLProviderArn']

    return {
        'Id': saml_provider_arn
    }



def delete_resource(physical_resource_id):
    """Deletes a SAML Provider in IAM.

    Parameters:
    ----------
    - physical_resource_id [string]: The Id of the Routing Profile.

    Return:
    ------
    Dictionary (with Id of the Lambda)
    """

    if physical_resource_id is None or physical_resource_id == '':
        return

    try:
        iam.delete_saml_provider(SAMLProviderArn = physical_resource_id)
        logger.info(f'Deleted SAML Provider {physical_resource_id}')
    except Exception as e:
        logger.exception(f'Failed to delete SAML Provider {physical_resource_id}')

    return