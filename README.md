# AWS CloudFormation Custom Resource Deployer

## Description

This solution was developed to help deploy and configure AWS services that are not supported by CloudFormation. Originally, this was created to enable the Infrastructure-as-Code (IaC) paradigm as CloudFormation does not fully support Amazon Connect. There


## Supported Services

Currently, this supports the following services:

### Amazon Connect

Supports the following operations with Amazon Connect:
- Instance*
- Hours of Operation*
- Queue*
- Routing Profile
- Lambda Association*
- Origin Association*

**IMPORTANT NOTE: At the point of time in which this was written, most of the Amazon Connect APIs are only in preview from AWS (denoted by an asterisk).**

### Identity & Access Management

Supports the following operations with IAM:
- SAML Provider (using federation metadata URL)

## Deployment Guide
Please refer to [deployment guide](docs/01-deployment.md).