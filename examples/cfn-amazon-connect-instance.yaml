AWSTemplateFormatVersion: 2010-09-09

Description: Deploys and configures the Amazon Connect instance.


Parameters:
  CustomResourceDeployerArn:
    Description: The ARN of the Lambda used to deploy custom resources.
    Type: String

  ConnectInstanceAlias:
    Description: The name of the Amazon Connect instance. If this is changed after the initial deployment, only the name of the S3 bucket will be updated, the instance will not be updated.
    Type: String

  ConnectDirectory:
    Description: The user directory to use with the Amazon Connect instance. If this is changed after the initial deployment, the instance will not be updated.
    Type: String
    AllowedValues:
      - CONNECT_MANAGED
      - SAML
      - EXISTING_DIRECTORY

  EnableContactFlowLogs:
    Description: Enable contact flow logs for the Amazon Connect instance.
    Type: String
    AllowedValues:
      - 'true'
      - 'false'
    Default: 'true'

  EnableContactLens:
    Description: Enable contact lens for the Amazon Connect instance.
    Type: String
    AllowedValues:
      - 'true'
      - 'false'
    Default: 'true'

  EnableInboundCalling:
    Description: Enable inbound voice calling for the Amazon Connect instance.
    Type: String
    AllowedValues:
      - 'true'
      - 'false'
    Default: 'true'

  EnableOutboundCalling:
    Description: Disable outbound voice calling for the Amazon Connect instance.
    Type: String
    AllowedValues:
      - 'true'
      - 'false'
    Default: 'true'

  CallRecordingsPrefix:
    Description: The S3 prefix used for call recordings.
    Type: String
    Default: CallRecordings

  ChatTranscriptsPrefix:
    Description: The S3 prefix used for chat transcripts.
    Type: String
    Default: ChatTranscripts

  SamlMetadataDocumentUrl:
    Description: The URL to the SAML Metadata XML Document.
    Type: String


Resources:
  S3KmsKey:
    Type: AWS::KMS::Key
    Properties:
      Description: KMS key that protects S3.
      Enabled: True
      KeyPolicy:
        Version: 2012-10-17
        Statement:
          - Sid: Full permission for root account.
            Effect: Allow
            Principal:
              AWS:
                - !Sub arn:aws:iam::${AWS::AccountId}:root
            Action:
              - kms:Create*
              - kms:Describe*
              - kms:Enable*
              - kms:List*
              - kms:Put*
              - kms:Update*
              - kms:Revoke*
              - kms:Disable*
              - kms:Get*
              - kms:Delete*
              - kms:TagResource
              - kms:UntagResource
              - kms:ScheduleKeyDeletion
              - kms:CancelKeyDeletion
            Resource: '*'
          - Sid: Allow access through S3 for all principals in the account that are authorized to use S3
            Effect: Allow
            Principal:
              AWS: '*'
            Action:
            - kms:Encrypt
            - kms:Decrypt
            - kms:ReEncrypt*
            - kms:GenerateDataKey*
            - kms:DescribeKey
            Resource: '*'
            Condition:
              StringEquals:
                kms:ViaService: !Sub s3.${AWS::Region}.amazonaws.com
                kms:CallerAccount: !Sub ${AWS::AccountId}
          - Sid: Allow direct access to key metadata to the account
            Effect: Allow
            Principal:
              AWS: !Sub arn:aws:iam::${AWS::AccountId}:root
            Action:
            - kms:Describe*
            - kms:Get*
            - kms:List*
            Resource: '*'

  S3KmsAlias:
    Type: AWS::KMS::Alias
    Properties:
      AliasName: alias/ConnectS3
      TargetKeyId: !GetAtt S3KmsKey.Arn

  KinesisKmsKey:
    Type: AWS::KMS::Key
    Properties:
      Description: KMS key that protects Kinesis Data Streams.
      Enabled: True
      KeyPolicy:
        Version: 2012-10-17
        Statement:
          - Sid: Full permission for root account.
            Effect: Allow
            Principal:
              AWS:
                - !Sub arn:aws:iam::${AWS::AccountId}:root
            Action:
              - kms:Create*
              - kms:Describe*
              - kms:Enable*
              - kms:List*
              - kms:Put*
              - kms:Update*
              - kms:Revoke*
              - kms:Disable*
              - kms:Get*
              - kms:Delete*
              - kms:TagResource
              - kms:UntagResource
              - kms:ScheduleKeyDeletion
              - kms:CancelKeyDeletion
            Resource: '*'
          - Sid: Allow access through Amazon Kinesis for all principals in the account that are authorized to use Amazon Kinesis
            Effect: Allow
            Principal:
              AWS: '*'
            Action:
            - kms:Encrypt
            - kms:Decrypt
            - kms:ReEncrypt*
            - kms:GenerateDataKey*
            - kms:DescribeKey
            Resource: '*'
            Condition:
              StringEquals:
                kms:ViaService: !Sub kinesis.${AWS::Region}.amazonaws.com
                kms:CallerAccount: !Sub ${AWS::AccountId}
          - Sid: Allow direct access to key metadata to the account
            Effect: Allow
            Principal:
              AWS: !Sub arn:aws:iam::${AWS::AccountId}:root
            Action:
            - kms:Describe*
            - kms:Get*
            - kms:List*
            Resource: '*'

  KinesisKmsAlias:
    Type: AWS::KMS::Alias
    Properties:
      AliasName: alias/ConnectKinesis
      TargetKeyId: !GetAtt KinesisKmsKey.Arn

  ConnectKmsKey:
    Type: AWS::KMS::Key
    Properties:
      Description: KMS key that protects Connect.
      Enabled: True
      KeyPolicy:
        Version: 2012-10-17
        Statement:
          - Sid: Full permission for root account.
            Effect: Allow
            Principal:
              AWS:
                - !Sub arn:aws:iam::${AWS::AccountId}:root
            Action:
              - kms:Create*
              - kms:Describe*
              - kms:Enable*
              - kms:List*
              - kms:Put*
              - kms:Update*
              - kms:Revoke*
              - kms:Disable*
              - kms:Get*
              - kms:Delete*
              - kms:TagResource
              - kms:UntagResource
              - kms:ScheduleKeyDeletion
              - kms:CancelKeyDeletion
            Resource: '*'
          - Sid: Allow access through Connect for all principals in the account that are authorized to use Connect
            Effect: Allow
            Principal:
              AWS: '*'
            Action:
            - kms:CreateGrant
            - kms:Describe*
            - kms:Get*
            - kms:List*
            Resource: '*'
            Condition:
              StringEquals:
                kms:ViaService: !Sub connect.${AWS::Region}.amazonaws.com
                kms:CallerAccount: !Sub ${AWS::AccountId}
          - Sid: Allow Connect to directly describe the key
            Effect: Allow
            Principal:
              Service: connect.amazonaws.com
            Action:
            - kms:Describe*
            - kms:Get*
            - kms:List*
            Resource: '*'
          - Sid: Allow direct access to key metadata to the account
            Effect: Allow
            Principal:
              AWS: !Sub arn:aws:iam::${AWS::AccountId}:root
            Action:
            - kms:Describe*
            - kms:Get*
            - kms:List*
            - kms:RevokeGrant
            Resource: '*'
          - Sid: Restrict direct usage
            Effect: Deny
            Principal:
              AWS: '*'
            Action:
            - kms:Encrypt
            - kms:GenerateDataKey*
            - kms:ReEncrypt*
            Resource: '*'
            Condition:
              'Null':
                kms:ViaService: 'true'
          - Sid: Restrict usage to Connect data
            Effect: Deny
            Principal:
              AWS: '*'
            Action:
            - kms:Encrypt
            - kms:GenerateDataKey*
            - kms:ReEncrypt*
            Resource: '*'
            Condition:
              'Null':
                kms:EncryptionContext:aws:connect:OrganizationId: 'true'
          - Sid: Allow access through S3 for all principals in the account that are authorized to use S3
            Effect: Allow
            Principal:
              AWS: '*'
            Action: kms:Decrypt
            Resource: '*'
            Condition:
              StringEquals:
                kms:ViaService: !Sub s3.${AWS::Region}.amazonaws.com
                kms:CallerAccount: !Sub ${AWS::AccountId}
          - Sid: Allow access through KVS for all principals in the account that are authorized to use KVS
            Effect: Allow
            Principal:
              AWS: '*'
            Action: kms:Decrypt
            Resource: '*'
            Condition:
              StringEquals:
                kms:ViaService: !Sub kinesisvideo.${AWS::Region}.amazonaws.com
                kms:CallerAccount: !Sub ${AWS::AccountId}

  ConnectKmsAlias:
    Type: AWS::KMS::Alias
    Properties:
      AliasName: alias/Connect
      TargetKeyId: !GetAtt ConnectKmsKey.Arn

  ConnectBucket:
    # DeletionPolicy: Retain
    # UpdateReplacePolicy: Retain
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub
        - amzconnect-${UniqueString}
        - { UniqueString: !Select [ 4, !Split [ '-', !Select [ 2, !Split [ '/', !Ref AWS::StackId ] ] ] ] }
      AccessControl: BucketOwnerFullControl
      BucketEncryption:
        ServerSideEncryptionConfiguration:
          - ServerSideEncryptionByDefault:
              SSEAlgorithm: aws:kms
              KMSMasterKeyID: !GetAtt S3KmsKey.Arn
      PublicAccessBlockConfiguration:
        BlockPublicAcls: True
        BlockPublicPolicy: True
        IgnorePublicAcls: True
        RestrictPublicBuckets: True

  ConnectCtrStream:
    # DeletionPolicy: Retain
    # UpdateReplacePolicy: Retain
    Type: AWS::Kinesis::Stream
    Properties:
      ShardCount: 1
      StreamEncryption:
        EncryptionType: KMS
        KeyId: !GetAtt KinesisKmsKey.Arn

  ConnectAgentStream:
    # DeletionPolicy: Retain
    # UpdateReplacePolicy: Retain
    Type: AWS::Kinesis::Stream
    Properties:
      ShardCount: 1
      StreamEncryption:
        EncryptionType: KMS
        KeyId: !GetAtt KinesisKmsKey.Arn

  ConnectInstance:
    Type: Custom::ConnectInstance
    Properties:
      ServiceToken: !Ref CustomResourceDeployerArn
      Alias: !Ref ConnectInstanceAlias
      Directory: !Ref ConnectDirectory
      EnableContactFlowLogs: !Ref EnableContactFlowLogs
      EnableContactLens: !Ref EnableContactLens
      EnableInboundCalling: !Ref EnableInboundCalling
      EnableOutboundCalling: !Ref EnableOutboundCalling
      BucketName: !Ref ConnectBucket
      CallRecordingsPrefix: !Ref CallRecordingsPrefix
      ChatTranscriptsPrefix: !Ref ChatTranscriptsPrefix
      KmsArn: !GetAtt ConnectKmsKey.Arn
      CtrStreamArn: !GetAtt ConnectCtrStream.Arn
      AgentStreamArn: !GetAtt ConnectAgentStream.Arn

  ConnectHoursOfOperation:
    Type: Custom::ConnectHoursOfOperation
    Properties:
      ServiceToken: !Ref CustomResourceDeployerArn
      InstanceId: !Ref ConnectInstance
      Name: Regular Business Hours
      Description: Regular hours for the business.
      TimeZone: Australia/Melbourne
      Monday: 09:00-17:00
      Tuesday: 09:00-17:00
      Wednesday: 09:00-17:00
      Thursday: 09:00-17:00
      Friday: 09:00-12:00,14:00-16:00
      Saturday: 09:00-12:00


  ConnectInstanceSamlProvider:
    Type: Custom::IamSamlProvider
    Properties:
      ServiceToken: !Ref CustomResourceDeployerArn
      Name: !Sub ${ConnectInstanceAlias}-saml-provider
      SamlMetadataDocumentUrl: !Ref SamlMetadataDocumentUrl

  ConnectInstanceFederationRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Sub ${ConnectInstanceAlias}-federation-role
      AssumeRolePolicyDocument:
        Statement:
          - Action: sts:AssumeRoleWithSAML
            Effect: Allow
            Principal:
              Federated: !Ref ConnectInstanceSamlProvider
            Condition:
              StringEquals:
                SAML:aud: https://signin.aws.amazon.com/saml
      Policies:
        - PolicyName: ConnectFederationPolicy
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Action:
                  - connect:GetFederationToken
                Effect: Allow
                Resource: '*'
                Condition:
                  StringEquals:
                    connect:InstanceId: !Ref ConnectInstance
              - Action:
                  - connect:GetFederationToken
                Effect: Allow
                Resource:
                  - !Join 
                    - ''
                    - - !GetAtt ConnectInstance.Arn
                      - /user/${aws:userid}


Outputs:
  ConnectInstanceId:
    Description: The id of the Amazon Connect instance.
    Value: !Ref ConnectInstance

  ConnectInstanceArn:
    Description: The ARN of the Amazon Connect instance.
    Value: !GetAtt ConnectInstance.Arn

  ConnectInstanceAlias:
    Description: The name / alias of the Amazon Connect instance.
    Value: !Ref ConnectInstanceAlias

  ConnectSamlClaimRole:
    Description: The role to use as a SAML claim in the identity provider.
    Value: !Sub 
      - ${ConnectInstanceSamlProviderArn},${ConnectInstanceFederationRoleArn}
      - { ConnectInstanceSamlProviderArn: !Ref ConnectInstanceSamlProvider, ConnectInstanceFederationRoleArn: !GetAtt ConnectInstanceFederationRole.Arn }

  ConnectSamlRelayState:
    Description: The relay state to use in the identity provider.
    Value: !Sub https://${AWS::Region}.console.aws.amazon.com/connect/federate/${ConnectInstance}?destination=%2Fconnect%2Fhome