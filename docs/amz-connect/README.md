# Amazon Connect

The CloudFormation Custom Deployer module supports the following operations on Amazon Connect:

| Name | Description | Notes |
|---|---|---|
| [Custom::ConnectInstance](instance.md) | Creates, updates and deletes an Amazon Connect instance. | |
| [Custom::ConnectHoursOfOperation](hours-of-operation.md) | Creates, updates and deletes Hours of Operations in an Amazon Connect instance. | |
| [Custom::ConnectQueue](queue.md) | Creates, updates and 'deletes' Queues in an Amazon Connect instance. | Queues cannot be deleted in Amazon Connect. When removed from CloudFormation, this will rename the queue. | |
| [Custom::ConnectRoutingProfile](routing-profile.md) | Creates, updates and 'deletes' Routing Profiles in an Amazon Connect instance. | Routing Profiles cannot be deleted in Amazon Connect. When removed from CloudFormation, this will rename the queue. | |
| [Custom::ConnectRoutingProfileQueue](routing-profile-queue.md) | Associates a Queue to a Routing Profile in an Amazon Connect instance. | | 
| [Custom::ConnectLambdaAssociation](lambda-association.md) | Associates and disassociates a Lambda from an Amazon Connect instance. | |
| [Custom::ConnectOriginAssociation](origin-association.md) | Associates and disassociates an Origin from an Amazon Connect instance. | |