# Amazon Connect

## Custom::ConnectRoutingProfileQueue (Routing Profile Queue Association)

Associates a Queue to a Routing Profile in an Amazon Connect instance.


### Syntax
To declare this entity in your AWS CloudFormation template, use the following syntax:

```
ConnectRoutingProfileQueue:
  Type: Custom::ConnectRoutingProfileQueue
  Properties:
    ServiceToken: String
    InstanceId: String
    RoutingProfileId: String
    QueueId: String
    Channel: String
    Priority: String
    Delay: String
```


### Properties

**ServiceToken**\
The ARN of the Custom Resource Deployer Lambda.\
*Required:* Yes\
*Type:* String
\
\
**InstanceId**\
The Amazon Connect instance id that the routing profile will be created in.\
*Required:* Yes\
*Type:* String
\
\
**RoutingProfileId**\
The id of the routing profile to associate the queue with.\
*Required:* Yes\
*Type:* String
\
\
**QueueId**\
The id for the queue to be associated with the routing profile.\
*Required:* Yes\
*Type:* String
\
\
**Channel**\
The queue to be used for outbound calls.\
*Required:* Yes\
*Type:* String\
*Accepted Values:* VOICE | CHAT | TASK
\
\
**Priority**\
The priority of the queue.\
*Required:* Yes\
*Type:* Integer
\
\
**Delay**\
The delay (in seconds) to associate to the queue.\
*Required:* Yes\
*Type:* Integer


### Return Values

**Ref**\
When you pass the logical ID of this resource to the intrinsic Ref function, Ref returns an ARN formatted string.


### Example

Create an Amazon Connect routing profile called ```Regular Routing Profile```.

```
ConnectRoutingProfileQueue:
  Type: Custom::ConnectRoutingProfileQueue
  Properties:
    ServiceToken: arn:aws:lambda:ap-southeast-2:123456789012:function:CfnCustomResourceDeployer
    InstanceId: 36cb8cf1-82a4-4f60-a482-4a19f229928b
    RoutingProfileId: c0c073e9-9f13-4b00-b506-a83045403b3d
    QueueId: 3b658df4-6bbc-4637-8504-15d9f063b641
    Channel: VOICE
    Priority: 1
    Delay: 30
```