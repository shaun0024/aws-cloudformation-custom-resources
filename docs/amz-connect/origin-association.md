# Amazon Connect

## Custom::ConnectOriginAssociation (Origin Association)

Associates an approved origin to an Amazon Connect instance.


### Syntax
To declare this entity in your AWS CloudFormation template, use the following syntax:

```
ConnectOriginAssociation:
  Type: Custom::ConnectOriginAssociation
  Properties:
    ServiceToken: String
    InstanceId: String
    Origin: String
```


### Properties

**ServiceToken**\
The ARN of the Custom Resource Deployer Lambda.\
*Required:* Yes\
*Type:* String
\
\
**InstanceId**\
The Amazon Connect instance id that the hours of operation will be created in.\
*Required:* Yes\
*Type:* String
\
\
**Origin**
The domain to add to the Amazon Connect allow list.\
*Required:* Yes\
*Type:* String


### Return Values

**Ref**\
When you pass the logical ID of this resource to the intrinsic Ref function, Ref returns the origin.


### Example

Associate a Salesforce domain called ```my.salesforce.com```.

```
ConnectOriginAssociation:
  Type: Custom::ConnectOriginAssociation
  Properties:
    ServiceToken: arn:aws:lambda:ap-southeast-2:123456789012:function:CfnCustomResourceDeployer
    InstanceId: 36cb8cf1-82a4-4f60-a482-4a19f229928b
    Origin: https://my.salesforce.com
```