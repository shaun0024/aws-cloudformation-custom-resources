# Amazon Connect

## Custom::ConnectRoutingProfile (Routing Profile)

Creates a Routing Profile in an Amazon Connect instance.

This can also 'import' an existing Routing Profile in an Amazon Connect instance that was deployed before to be managed as code moving forward. This can be done by providing the ```Name``` of the existing queue. Ensure that all the other settings are as is, otherwise it may be modified.

***IMPORTANT NOTE:*** Amazon Connect does not support deletion of routing profiles. If the routing profile is 'deleted' in CloudFormation, the routing profile will be prefixed with a 'z' and suffixed with a random 8 character string. 

### Syntax
To declare this entity in your AWS CloudFormation template, use the following syntax:

```
ConnectRoutingProfile:
  Type: Custom::ConnectRoutingProfile
  Properties:
    ServiceToken: String
    InstanceId: String
    Name: String
    Description:
    OutboundQueueId: String
    VoiceConcurrency: Integer
    ChatConcurrency: Integer
    TaskConcurrency: Integer
```


### Properties

**ServiceToken**\
The ARN of the Custom Resource Deployer Lambda.\
*Required:* Yes\
*Type:* String
\
\
**InstanceId**\
The Amazon Connect instance id that the routing profile will be created in.\
*Required:* Yes\
*Type:* String
\
\
**Name**\
The name of the routing profile.\
*Required:* Yes\
*Type:* String
\
\
**Description**\
The description for the routing profile.\
*Required:* Yes\
*Type:* String
\
\
**OutboundQueueId**\
The queue to be used for outbound calls.\
*Required:* Yes\
*Type:* String\
\
\
**VoiceConcurrency**\
The number of concurrent voice contacts that an agent can attend to. **IMPORTANT NOTE: Voice will always have 1 concurrency. If this was provided originally or during a later change but is no longer needed, replace the value with an empty string (i.e. '') as the original value will continue to be passed by CloudFormation.**\
*Required:* No\
*Type:* Integer
\
\
**ChatConcurrency**\
The number of concurrent chat contacts that an agent can attend to. **IMPORTANT NOTE: If this was provided originally or during a later change but is no longer needed, replace the value with an empty string (i.e. '') as the original value will continue to be passed by CloudFormation.**\
*Required:* No\
*Type:* Integer (1 to 10)
\
\
**TaskConcurrency**\
The number of concurrent task contacts that an agent can attend to. **IMPORTANT NOTE: If this was provided originally or during a later change but is no longer needed, replace the value with an empty string (i.e. '') as the original value will continue to be passed by CloudFormation.**\
*Required:* No\
*Type:* Integer (1 to 10)


### Return Values

**Ref**\
When you pass the logical ID of this resource to the intrinsic Ref function, Ref returns the id of the routing profile.

**Fn::GetAtt**\
The Fn::GetAtt intrinsic function returns a value for a specified attribute of this type. The following are the available attributes.\
*Arn:* The ARN of the routing profile.


### Example

Create an Amazon Connect routing profile called ```Regular Routing Profile```.

```
ConnectRoutingProfile:
  Type: Custom::ConnectRoutingProfile
  Properties:
    ServiceToken: arn:aws:lambda:ap-southeast-2:123456789012:function:CfnCustomResourceDeployer
    InstanceId: 36cb8cf1-82a4-4f60-a482-4a19f229928b
    Name: Regular Routing Profile
    Description: Regular routing profile.
    OutboundQueueId: c0c073e9-9f13-4b00-b506-a83045403b3d
    VoiceConcurrency: 1
    ChatConcurrency: 5
    TaskConcurrency: 5
```