# Amazon Connect

## Custom::ConnectQueue (Hours of Operation)

Creates a Queue in an Amazon Connect instance.

This can also 'import' an existing Queue in an Amazon Connect instance that was deployed before to be managed as code moving forward. This can be done by providing the ```Name``` of the existing queue. Ensure that all the other settings are as is, otherwise it may be modified.

***IMPORTANT NOTE:*** Amazon Connect does not support deletion of queues. If the queue is 'deleted' in CloudFormation, the queue will be prefixed with a 'z' and suffixed with a random 8 character string. 


### Syntax
To declare this entity in your AWS CloudFormation template, use the following syntax:

```
ConnectHoursOfOperation:
  Type: Custom::ConnectQueue
  Properties:
    ServiceToken: String
    InstanceId: String
    Name: String
    Description: String
    HoursOfOperationId: String
    MaxContacts: String
    Status: String
    OutboundCallerIdName: String
    OutboundCallerIdNumberId: String
    OutboundFlowId: String
```


### Properties

**ServiceToken**\
The ARN of the Custom Resource Deployer Lambda.\
*Required:* Yes\
*Type:* String
\
\
**InstanceId**\
The Amazon Connect instance id that the hours of operation will be created in.\
*Required:* Yes\
*Type:* String
\
\
**Name**\
The name of the queue.\
*Required:* Yes\
*Type:* String\
\
\
**Description**\
The description for the queue.\
*Required:* Yes\
*Type:* String\
\
\
**HoursOfOperationId**\
The id for the hours of operation to associate with the queue.\
*Required:* Yes\
*Type:* String\
\
\
**MaxContacts**\
The maximum number of contacts that the queue can handle. This must be between 0 (no maximum) to 9. **IMPORTANT NOTE: If this was provided originally or during a later change but is no longer needed, replace the value with an empty string (i.e. '') as the original value will continue to be passed by CloudFormation.**\
*Required:* No\
*Type:* String
\
\
**Status**\
The maximum number of contacts that the queue can handle. **IMPORTANT NOTE: If this was provided originally or during a later change but is no longer needed, replace the value with an empty string (i.e. '') as the original value will continue to be passed by CloudFormation.**\
*Required:* No\
*Type:* String\
*Accepted Values:* ENABLED | DISABLED
\
\
**OutboundCallerIdName**\
The caller id used for outbound calls. **IMPORTANT NOTE: If this was provided originally or during a later change but is no longer needed, replace the value with an empty string (i.e. '') as the original value will continue to be passed by CloudFormation.**\
*Required:* No\
*Type:* String
\
\
**OutboundCallerIdNumberId**\
The number used for outbound calls. This must be the id of the number (not the phone number) already assigned to Amazon Connect. **IMPORTANT NOTE: If this was provided originally or during a later change but is no longer needed, replace the value with an empty string (i.e. '') as the original value will continue to be passed by CloudFormation.**\
*Required:* No\
*Type:* String
\
\
**OutboundFlowId**\
Time contact flow id used when making outbound calls. **IMPORTANT NOTE: If this was provided originally or during a later change but is no longer needed, replace the value with an empty string (i.e. '') as the original value will continue to be passed by CloudFormation.**\
*Required:* No\
*Type:* String


### Return Values

**Ref**\
When you pass the logical ID of this resource to the intrinsic Ref function, Ref returns the id of the queue.

**Fn::GetAtt**\
The Fn::GetAtt intrinsic function returns a value for a specified attribute of this type. The following are the available attributes.\
*Arn:* The ARN of the queue.


### Example

Create a queue called ```Regular Queue``` that:
- will queue a maximum of 10 contacts
- 9am to 12pm and 2pm to 4pm on Friday
- 9am to 12pm on Saturday

```
ConnectHoursOfOperation:
  Type: Custom::ConnectQueue
  Properties:
    ServiceToken: arn:aws:lambda:ap-southeast-2:123456789012:function:CfnCustomResourceDeployer
    InstanceId: 36cb8cf1-82a4-4f60-a482-4a19f229928b
    Name: Regular Queue
    Description: Queue for regular contacts
    HoursOfOperationId: b6bc1605-a69f-49de-860f-5cfd54a71d1f
    MaxContacts: 10
    Status: ENABLED
    OutboundCallerIdName: MyBusinessName
    OutboundCallerIdNumberId: String
    OutboundFlowId: String
```