# Amazon Connect

## Custom::ConnectInstance (Instance)

Creates an Amazon Connect instance.

This can also 'import' an existing Amazon Connect instance that was deployed before to be managed as code moving forward. This can be done by providing the ```Alias``` of the existing instance. Ensure that all the other settings are as is, otherwise it may be modified.


### Syntax
To declare this entity in your AWS CloudFormation template, use the following syntax:

```
ConnectInstance:
  Type: Custom::ConnectInstance
  Properties:
    ServiceToken: String
    Alias: String
    Directory: String
    EnableInboundCalling: String
    EnableOutboundCalling: String
    BucketName: String
    CallRecordingsPrefix: String
    ChatTranscriptsPrefix: String
    EnableContactFlowLogs: String
    EnableContactLens: String
    KmsArn: String
    CtrStreamArn: String
    AgentStreamArn: String
```


### Properties

**ServiceToken**\
The ARN of the Custom Resource Deployer Lambda.\
*Required:* Yes\
*Type:* String
\
\
**Alias**\
The name for the Amazon Connect instance.\
*Required:* Yes\
*Type:* String
\
\
**Directory**\
The type of directory to be used by the Amazon Connect instance to manage users. ***IMPORTANT NOTE:*** Updating this has no effect after the instance has been created.\
*Required:* Yes\
*Type:* String\
*Accepted Values:* SAML | CONNECT_MANAGED
\
\
**EnableInboundCalling**\
Allow the Amazon Connect instance to accept inbound calls.\
*Required:* No\
*Type:* String\
*Accepted Values:* true | false\
*Default:* false
\
\
**EnableOutboundCalling**\
Allow the Amazon Connect instance to make outbound calls.\
*Required:* No\
*Type:* String\
*Accepted Values:* true | false\
*Default:* false
\
\
**BucketName**\
The S3 bucket used to store Amazon Connect data (i.e. transcripts, recordings, reports). This does not create a new S3 bucket; it must have been created earlier.\
*Required:* Yes\
*Type:* String
\
\
**CallRecordingsPrefix**\
The prefix to be used on call recordings when stored in the S3 bucket.\
*Required:* No\
*Type:* String\
*Default:* connect/{instance_alias}/CallRecordings
\
\
**ChatTranscriptsPrefix**\
The prefix to be used on chat transcripts when stored in the S3 bucket.\
*Required:* No\
*Type:* Boolean\
*Default:* connect/{instance_alias}/ChatTranscripts
\
\
**EnableContactFlowLogs**\
Enables logging of contact flows into CloudWatch Logs.\
*Required:* No\
*Type:* String\
*Accepted Values:* true | false\
*Default:* true
\
\
**EnableContactLens**\
Enables Contact Lens for contact analysis.\
*Required:* No\
*Type:* String\
*Accepted Values:* true | false\
*Default:* false
\
\
**KmsArn**\
The ARN of KMS key id to use to encrypt call recordings and chat transcripts. This does not create a new KMS key; it must have been created earlier. **IMPORTANT NOTE: If this was provided originally or during a later change but is no longer needed, replace the value with an empty string (i.e. '') as the original value will continue to be passed by CloudFormation.**\
*Required:* No\
*Type:* String\
*Default:* None
\
\
**CtrStreamArn**\
The ARN of Kinesis Data Stream to use for Contact Trace Records. This does not create a new Kinesis Data Stream; it must have been created earlier. **IMPORTANT NOTE: If this was provided originally or during a later change but is no longer needed, replace the value with an empty string (i.e. '') as the original value will continue to be passed by CloudFormation.**\
*Required:* No\
*Type:* String\
*Default:* None
\
\
**AgentStreamArn**\
The ARN of Kinesis Data Stream to use for Agent events. This does not create a new Kinesis Data Stream; it must have been created earlier. **IMPORTANT NOTE: If this was provided originally or during a later change but is no longer needed, replace the value with an empty string (i.e. '') as the original value will continue to be passed by CloudFormation.**\
*Required:* No\
*Type:* String\
*Default:* None


### Return Values

**Ref**\
When you pass the logical ID of this resource to the intrinsic Ref function, Ref returns the id of the instance.

**Fn::GetAtt**\
The Fn::GetAtt intrinsic function returns a value for a specified attribute of this type. The following are the available attributes.\
*Arn:* The ARN of the instance


### Example

Create an Amazon Connect instance called ```my-amazon-connect```.

```
ConnectInstance:
  Type: Custom::ConnectInstance
  Properties:
    ServiceToken: arn:aws:lambda:ap-southeast-2:123456789012:function:CfnCustomResourceDeployer
    Alias: my-amazon-connect
    Directory: SAML
    EnableContactFlowLogs: 'true'
    EnableContactLens: 'true'
    EnableInboundCalling: 'true'
    EnableOutboundCalling: 'true'
    BucketName: my-amazon-connect-bucket
    CallRecordingsPrefix: CallRecordings
    ChatTranscriptsPrefix: ChatTranscripts
    KmsArn: arn:aws:kms:ap-southeast-2:123456789012:key/f6c09f73-83bc-4676-b68d-9cff4e8afce3
    CtrStreamArn: arn:aws:kinesis:ap-southeast-2:123456789012:stream/ConnectCTRStream
    AgentStreamArn: arn:aws:kinesis:ap-southeast-2:123456789012:stream/ConnectAgentStream
```