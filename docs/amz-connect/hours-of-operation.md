# Amazon Connect

## Custom::ConnectHoursOfOperation (Hours of Operation)

Creates an Hours of Operation in an Amazon Connect instance.

This can also 'import' an existing Hours of Operation in an Amazon Connect instance that was deployed before to be managed as code moving forward. This can be done by providing the ```Name``` of the existing hours of operation. Ensure that all the other settings are as is, otherwise it may be modified.


### Syntax
To declare this entity in your AWS CloudFormation template, use the following syntax:

```
ConnectHoursOfOperation:
  Type: Custom::ConnectHoursOfOperation
  Properties:
    ServiceToken: String
    InstanceId: String
    Name: String
    Description: String
    TimeZone: String
    Monday: String
    Tuesday: String
    Wednesday: String
    Thursday: String
    Friday: String
    Saturday: String
    Sunday: String
```


### Properties

**ServiceToken**\
The ARN of the Custom Resource Deployer Lambda.\
*Required:* Yes\
*Type:* String
\
\
**InstanceId**\
The Amazon Connect instance id that the hours of operation will be created in.\
*Required:* Yes\
*Type:* String
\
\
**Name**\
The name of the hours of operation.\
*Required:* Yes\
*Type:* String
\
\
**Description**\
The description for the hours of operation.\
*Required:* Yes\
*Type:* String
\
\
**Timezone**\
The timezone for the hours of operation. This must be from the IANA timezone database (e.g. Australia/Melbourne)\
*Required:* Yes\
*Type:* String\
*Accepted Values:* Refer to this [link](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones).
\
\
**Monday**\
Time slots for Monday. Multiple slots can be declared using a comma separated format. **IMPORTANT NOTE: If this was provided originally or during a later change but is no longer needed, replace the value with an empty string (i.e. '') as the original value will continue to be passed by CloudFormation.**\
*Required:* No\
*Type:* String\
*Required Format:* {start-time-24-hour-format}-{end-time-24-hour-format},{start-time-24-hour-format}-{end-time-24-hour-format}
\
\
**Tuesday**\
Time slots for Monday. Multiple slots can be declared using a comma separated format. **IMPORTANT NOTE: If this was provided originally or during a later change but is no longer needed, replace the value with an empty string (i.e. '') as the original value will continue to be passed by CloudFormation.**\
*Required:* No\
*Type:* String\
*Required Format:* {start-time-24-hour-format}-{end-time-24-hour-format},{start-time-24-hour-format}-{end-time-24-hour-format}
\
\
**Wednesday**\
Time slots for Monday. Multiple slots can be declared using a comma separated format. **IMPORTANT NOTE: If this was provided originally or during a later change but is no longer needed, replace the value with an empty string (i.e. '') as the original value will continue to be passed by CloudFormation.**\
*Required:* No\
*Type:* String\
*Required Format:* {start-time-24-hour-format}-{end-time-24-hour-format},{start-time-24-hour-format}-{end-time-24-hour-format}
\
\
**Thursday**\
Time slots for Monday. Multiple slots can be declared using a comma separated format. **IMPORTANT NOTE: If this was provided originally or during a later change but is no longer needed, replace the value with an empty string (i.e. '') as the original value will continue to be passed by CloudFormation.**\
*Required:* No\
*Type:* String\
*Required Format:* {start-time-24-hour-format}-{end-time-24-hour-format},{start-time-24-hour-format}-{end-time-24-hour-format}
\
\
**Friday**\
Time slots for Monday. Multiple slots can be declared using a comma separated format. **IMPORTANT NOTE: If this was provided originally or during a later change but is no longer needed, replace the value with an empty string (i.e. '') as the original value will continue to be passed by CloudFormation.**\
*Required:* No\
*Type:* String\
*Required Format:* {start-time-24-hour-format}-{end-time-24-hour-format},{start-time-24-hour-format}-{end-time-24-hour-format}
\
\
**Saturday**\
Time slots for Monday. Multiple slots can be declared using a comma separated format. **IMPORTANT NOTE: If this was provided originally or during a later change but is no longer needed, replace the value with an empty string (i.e. '') as the original value will continue to be passed by CloudFormation.**\
*Required:* No\
*Type:* String\
*Required Format:* {start-time-24-hour-format}-{end-time-24-hour-format},{start-time-24-hour-format}-{end-time-24-hour-format}
\
\
**Sunday**\
Time slots for Monday. Multiple slots can be declared using a comma separated format. **IMPORTANT NOTE: If this was provided originally or during a later change but is no longer needed, replace the value with an empty string (i.e. '') as the original value will continue to be passed by CloudFormation.**\
*Required:* No\
*Type:* String\
*Required Format:* {start-time-24-hour-format}-{end-time-24-hour-format},{start-time-24-hour-format}-{end-time-24-hour-format}


### Return Values

**Ref**\
When you pass the logical ID of this resource to the intrinsic Ref function, Ref returns the id of the hours of operation.

**Fn::GetAtt**\
The Fn::GetAtt intrinsic function returns a value for a specified attribute of this type. The following are the available attributes.\
*Arn:* The ARN of the hours of operation


### Example

Create hours of operation called ```Regular Business Hours``` operating from:
- 9am to 5pm on Mondays to Thursdays
- 9am to 12pm and 2pm to 4pm on Friday
- 9am to 12pm on Saturday

```
ConnectHoursOfOperation:
  Type: Custom::ConnectHoursOfOperation
  Properties:
    ServiceToken: arn:aws:lambda:ap-southeast-2:123456789012:function:CfnCustomResourceDeployer
    InstanceId: 36cb8cf1-82a4-4f60-a482-4a19f229928b
    Name: Regular Business Hours
    Description: Regular hours for the business.
    TimeZone: Australia/Melbourne
    Monday: 09:00-17:00
    Tuesday: 09:00-17:00
    Wednesday: 09:00-17:00
    Thursday: 09:00-17:00
    Friday: 09:00-12:00,14:00-16:00
    Saturday: 09:00-12:00
```