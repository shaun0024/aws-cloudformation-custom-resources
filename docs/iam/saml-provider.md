# Identity & Access Management

## Custom::IamSamlProvider (Identity Provider)

Creates an Identity Provider in IAM using a federation metadata URL.


### Syntax
To declare this entity in your AWS CloudFormation template, use the following syntax:

```
IamSamlProvider:
  Type: Custom::IamSamlProvider
  Properties:
    ServiceToken: String
    SamlMetadataDocumentUrl: String
```


### Properties

**ServiceToken**\
The ARN of the Custom Resource Deployer Lambda.\
*Required:* Yes\
*Type:* String
\
\
**Name**\
The name of identity provider.\
*Required:* Yes\
*Type:* String
\
\
**SamlMetadataDocumentUrl**\
The URL to the federation metadata document.\
*Required:* Yes\
*Type:* String


### Return Values

**Ref**\
When you pass the logical ID of this resource to the intrinsic Ref function, Ref returns the ARN of the identity provider.


### Example

Create an IAM provider named ```Azure-App```.

```
IamSamlProvider:
  Type: Custom::IamSamlProvider
  Properties:
    ServiceToken: arn:aws:lambda:ap-southeast-2:123456789012:function:CfnCustomResourceDeployer
    Name: Azure-App
    SamlMetadataDocumentUrl: https://login.microsoftonline.com/fbe8acf2-10ce-4460-a6be-2f9077d5d1e6/federationmetadata/2007-06/federationmetadata.xml?appid=0616dd76-83c7-4049-9463-5d05d7f2609e
```