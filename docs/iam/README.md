# Identity & Access Management

The CloudFormation Custom Deployer module supports the following operations on IAM:

| Name | Description | Notes |
|---|---|---|
| [Custom::IamSamlProvider](saml-provider.md) | Creates, updates and deletes an Identity Provider in IAM using a federation metadata URL. | |