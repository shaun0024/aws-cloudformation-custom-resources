# AWS CloudFormation Custom Resource Deployer

## Table of Contents

1. [Deployment Guide](01-deployment.md)
2. Supported Services
    - [Amazon Connect](amz-connect/README.md)
    - [Identity & Access Management](iam/README.md)