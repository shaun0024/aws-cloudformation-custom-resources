# DEPLOYMENT

## Deploy the CloudFormation Stack
1. Review the parameters required for deployment in the table below.

| Parameter Name | Description |
|---|---|
| LoggingLevel | The logging level for the Lambdas. Accepted values are ```info```, ```error```, ```warning```, ```debug```. Default is ```info```. |

2. Once done, identify the region to be used and click on the link to launch the CloudFormation service page.

| Region | Launch Link |
|---|---|
| US East (N. Virginia) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://us-east-1.console.aws.amazon.com/cloudformation/home?region=us-east-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43216163/cloudformation-custom-resources.yaml&stackName=CloudFormationCustomResourceDeployer) |
| US West (Oregon) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://us-west-2.console.aws.amazon.com/cloudformation/home?region=us-west-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43216163/cloudformation-custom-resources.yaml&stackName=CloudFormationCustomResourceDeployer) |
| Canada (Central) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ca-central-1.console.aws.amazon.com/cloudformation/home?region=ca-central-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43216163/cloudformation-custom-resources.yaml&stackName=CloudFormationCustomResourceDeployer) |
| Asia Pacific (Seoul) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-northeast-2.console.aws.amazon.com/cloudformation/home?region=ap-northeast-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43216163/cloudformation-custom-resources.yaml&stackName=CloudFormationCustomResourceDeployer) |
| Asia Pacific (Singapore) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-southeast-1.console.aws.amazon.com/cloudformation/home?region=ap-southeast-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43216163/cloudformation-custom-resources.yaml&stackName=CloudFormationCustomResourceDeployer) |
| Asia Pacific (Sydney) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-southeast-2.console.aws.amazon.com/cloudformation/home?region=ap-southeast-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43216163/cloudformation-custom-resources.yaml&stackName=CloudFormationCustomResourceDeployer) |
| Asia Pacific (Tokyo) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-northeast-1.console.aws.amazon.com/cloudformation/home?region=ap-northeast-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43216163/cloudformation-custom-resources.yaml&stackName=CloudFormationCustomResourceDeployer) |
| Europe (Frankfurt) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-central-1.console.aws.amazon.com/cloudformation/home?region=eu-central-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43216163/cloudformation-custom-resources.yaml&stackName=CloudFormationCustomResourceDeployer) |
| Europe (London) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-west-2.console.aws.amazon.com/cloudformation/home?region=eu-west-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43216163/cloudformation-custom-resources.yaml&stackName=CloudFormationCustomResourceDeployer) |

